/*
 * ryzcontour.cpp
 *
 * Copyright 2024 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <math.h>
#include "opencv2/imgproc.hpp"

const double pairing_sigmoid_slope = 10;
const double pairing_similarity_threshold = 0.0;
const std::vector<double> corner_smoothing_kernel = {0.9/25.0, 2.1/25.0, 3.3/25.0, 4.0/25.0, 4.2/25.0, 4.1/25.0, 3.4/25.0, 1.9/25.0, 1.1/25.0};
//std::vector<double> kernel = {1.0/9.0, 2.0/9.0, 3.0/9.0, 2.0/9.0, 1.0/9.0};

extern int grey_level_threshold;
extern int thresholding_algorithm;
extern int structuring_element_radius;
extern double concavity_threshold;
extern double pairing_grain_thickness;
extern double maximum_grain_area;
extern double minimum_grain_area;

struct ConcavePoint
{
    int point_index;
    int contour_index;
    int x;
    int y;
    double dx;
    double dy;
    double concavity;
};


static std::vector<ConcavePoint> getConcavePoints(const std::vector<cv::Point>& contour, const int contour_index)
{
    std::vector<ConcavePoint> results;

    int imax = contour.size();
    if(imax <= 7)
        return results;
    std::vector<double> turn;
    turn.resize(imax);
    std::vector<double> filtered;
    filtered.resize(imax);


    for(int i = 0; i < imax; i++)
    {
        int inext = (i + 1) % imax;
        int iprev = (i + imax - 1) % imax;
        cv::Point pprev = contour[iprev]-contour[i];
        cv::Point pnext = contour[inext]-contour[i];
        double dot = pprev.ddot(pnext);
        double cro = pprev.cross(pnext);
        dot /= sqrt(pprev.ddot(pprev)*pnext.ddot(pnext));
        dot += 1;
        dot /= 2;
        if(cro > 0)
            turn[i] = dot;
        else if(cro < 0)
            turn[i] = -dot;
        else
            turn[i] = 0;
    }

    int jmax = corner_smoothing_kernel.size();
    for(int i = 0; i < imax; i++)
    {
        double convolution = 0.0;
        for(int j = 0; j < jmax; j++)
        {
            convolution += (turn[(i + j) % imax] * corner_smoothing_kernel[j]);
        }
        filtered[(i+jmax/2)%imax] = convolution;
    }

    if(concavity_threshold >= 0)
    {
        for(int i = 0; i < imax; i++)
        {
            if(filtered[i] >= concavity_threshold)
            {
                int inext = (i + 1) % imax;
                int iprev = (i + imax - 1) % imax;
                int inext2 = (i + 2) % imax;
                int iprev2 = (i + imax - 2) % imax;
                int inext3 = (i + 3) % imax;
                int iprev3 = (i + imax - 3) % imax;
                if(
                        filtered[i] > filtered[inext] && filtered[i] >= filtered[iprev] &&
                        filtered[i] > filtered[inext2] && filtered[i] >= filtered[iprev2] &&
                        filtered[i] > filtered[inext3] && filtered[i] >= filtered[iprev3])
                {
                    ConcavePoint concave;
                    concave.contour_index = contour_index;
                    concave.point_index = i;
                    concave.x = contour[i].x;
                    concave.y = contour[i].y;
                    concave.concavity = filtered[i];
                    results.push_back(concave);
                }
            }
        }
    }
    else
    {
        for(int i = 0; i < imax; i++)
        {
            if(filtered[i] <= concavity_threshold)
            {
                int inext = (i + 1) % imax;
                int iprev = (i + imax - 1) % imax;
                int inext2 = (i + 2) % imax;
                int iprev2 = (i + imax - 2) % imax;
                int inext3 = (i + 3) % imax;
                int iprev3 = (i + imax - 3) % imax;
                if(
                        filtered[i] < filtered[inext] && filtered[i] <= filtered[iprev] &&
                        filtered[i] < filtered[inext2] && filtered[i] <= filtered[iprev2] &&
                        filtered[i] < filtered[inext3] && filtered[i] <= filtered[iprev3])
                {
                    ConcavePoint concave;
                    concave.contour_index = contour_index;
                    concave.point_index = i;
                    concave.x = contour[i].x;
                    concave.y = contour[i].y;
                    concave.concavity = -filtered[i];
                    results.push_back(concave);
                }
            }
        }
    }

    int kmax = results.size();
    for(int k = 0; k < kmax; k++)
    {
        double sumx = 0.0;
        double sumy = 0.0;
        for(int j = 0; j < jmax; j++)
        {
            int d = 1+(jmax/2)/imax;
            int a = (results[k].point_index+j-jmax/2+d*imax)%imax;
            cv::Point vect = contour[results[k].point_index]-contour[a];
            sumx += ((double)vect.x * corner_smoothing_kernel[j]);
            sumy += ((double)vect.y * corner_smoothing_kernel[j]);
        }
        results[k].dx = sumx;
        results[k].dy = sumy;
    }
    return results;
}


static double pairingSimilarity(const ConcavePoint& p1, const ConcavePoint& p2)
{
    double disx = p1.x-p2.x;
    double disy = p1.y-p2.y;
    double distance = sqrt( disx*disx + disy*disy );
    if(distance < 5.0)
        return 1.0;
    double simdist = exp(pairing_sigmoid_slope*distance/pairing_grain_thickness-pairing_sigmoid_slope);
    simdist = 1.0 - (simdist / (simdist + 1.0));
    disx /= distance;
    disy /= distance;
    distance = sqrt(p1.dx*p1.dx + p1.dy*p1.dy);
    double simang1 = (p1.dx*disx+p1.dy*disy)/distance;
    if(simang1 < 0)
        simang1 *= -simang1;
    else simang1 = 0;
    distance = sqrt(p2.dx*p2.dx + p2.dy*p2.dy);
    double simang2 = (p2.dx*disx+p2.dy*disy)/distance;
    if(simang2 > 0)
        simang2 *= simang1;
    else simang2 = 0;
    return simang1*simang2*simdist;
}

struct PointPirs
{
    double similarity;
    int index1;
    int index2;
};


static std::vector<cv::Vec4i> findPairs(const std::vector<ConcavePoint>& points)
{
    std::vector<PointPirs> temp_pairs;
    std::vector<cv::Vec4i> pairs;

    int count = points.size();
    for(int i = 0; i < count-1; i++)
        for(int j = i+1; j < count; j++)
        {
            double similarity = pairingSimilarity(points[i], points[j]);
            if(similarity > pairing_similarity_threshold)
            {
                temp_pairs.push_back({similarity, i, j});
            }
        }
    int index  = -1;
    do
    {
        index = -1;
        double maxsim = 0;
        count = temp_pairs.size();
        for(int i = 0; i < count; i++)
        {
            if(maxsim < temp_pairs[i].similarity)
            {
                maxsim = temp_pairs[i].similarity;
                index = i;
            }
        }
        if(index >= 0)
        {
            int i1 = temp_pairs[index].index1;
            int i2 = temp_pairs[index].index2;
            pairs.push_back({points[i1].contour_index, points[i1].point_index,
                             points[i2].contour_index, points[i2].point_index});
            temp_pairs[index].similarity = -1.0;

            for(int i = 0; i < temp_pairs.size(); i++)
            {
                if(temp_pairs[i].similarity >= 0)
                {
                    if(
                            temp_pairs[i].index1 == i1 ||
                            temp_pairs[i].index1 == i2 ||
                            temp_pairs[i].index2 == i1 ||
                            temp_pairs[i].index2 == i2
                      )
                    {
                       temp_pairs[i].similarity = -1;
                    }
                }
            }
        }
    }while(index >= 0);
    return pairs;
}

static std::vector<cv::Vec4i> getAllPointPairs(const std::vector<std::vector<cv::Point>>& contours, const std::vector<cv::Vec4i>& hierarchy)
{
    std::vector<cv::Vec4i> pairs;
    std::vector<bool> already_used;
    int cmax = contours.size();
    already_used.resize(cmax, false);

    bool change = false;
    do
    {
        change = false;
        for(int c = 0; c < cmax; c++)
        {
            if(! already_used[c])
            {
                bool doit = false;
                int parent = hierarchy[c][3];
                if(parent >= 0)
                    if(already_used[parent])
                        doit = true;
                if(parent == -1)
                    doit = true;
                if(doit)
                {
                    already_used[c] = true;
                    change = true;
                    std::vector<ConcavePoint> points = getConcavePoints(contours[c], c);

                    for(int cc = 0; cc < cmax; cc++)
                    {
                        if(! already_used[cc])
                        {
                            int parent2 = hierarchy[cc][3];
                            if(parent2 == c)
                            {
                                already_used[cc] = true;
                                std::vector<ConcavePoint> points2 = getConcavePoints(contours[cc], cc);
                                points.insert(points.end(), points2.begin(), points2.end());
                            }
                        }
                    }
                    std::vector<cv::Vec4i> pairs2 = findPairs(points);
                    if(pairs2.size() > 0)
                        pairs.insert(pairs.end(), pairs2.begin(), pairs2.end());
                }
            }
        }
    }
    while(change);
    return pairs;
}



static std::vector<std::vector<cv::Point>> getUntiedContours(const std::vector<std::vector<cv::Point>>& contours, const std::vector<cv::Vec4i>& pairs)
{
    //pairs
    //{contour_index, point_index, contour_index, point_index}
    std::vector<std::vector<cv::Point>> untied;
    std::vector<int> contour_begin;
    //{contour_index, point_index, previous, next}
    std::vector<cv::Vec4i> single_chain;

    int cmax = contours.size();
    int pmax = pairs.size();
    int nmax = 0;
    contour_begin.resize(cmax);
    for(int c = 0; c < cmax; c++)
    {
        contour_begin[c] = nmax;
        nmax += contours[c].size();
    }
    single_chain.resize(nmax+pmax*2);

    int j = 0;
    for(int c = 0; c < cmax; c++)
    {
        int imax = contours[c].size();
        single_chain[j][0] = c;
        single_chain[j][1] = 0;
        single_chain[j][2] = j+imax-1;
        single_chain[j][3] = j+1;
        j++;
        for(int i = 1; i < imax-1; i++)
        {
            single_chain[j][0] = c;
            single_chain[j][1] = i;
            single_chain[j][2] = j-1;
            single_chain[j][3] = j+1;
            j++;
        }
        single_chain[j][0] = c;
        single_chain[j][1] = imax-1;
        single_chain[j][2] = j-1;
        single_chain[j][3] = j-imax+1;
        j++;
    }

//    for(int p = pmax-1; p >= 0; p--)
    for(int p = 0; p < pmax; p++)
    {
        int point1 = contour_begin [pairs[p][0]] + pairs[p][1];
        int point2 = contour_begin [pairs[p][2]] + pairs[p][3];
        single_chain[j] = single_chain[point1];
        single_chain[j][2] = j+1;//point2;
        single_chain[ single_chain[j][3] ][2] = j;
        j++;
        single_chain[j] = single_chain[point2];
        single_chain[j][3] = j-1;//point1;
        single_chain[ single_chain[j][2] ][3] = j;
        j++;
        single_chain[point1][3] = point2;
        single_chain[point2][2] = point1;
    }

    for(int p = 0; p < j; p++)
    {
        int pp = p;
        std::vector<cv::Point> newcont;
        while(single_chain[pp][0] >= 0)
        {
            newcont.push_back(contours[ single_chain[pp][0] ][single_chain[pp][1]] );

            single_chain[pp][0] = -1;
            pp = single_chain[pp][3];
        }
        if(newcont.size() > 0)
            untied.push_back(newcont);
    }
    return untied;
}

/*
void circularMedianFilter(const cv::Mat& src, cv::Mat& dst, const cv::Mat& circularMask)
{
    int radius = circularMask.cols / 2;
    dst = src.clone();
    cv::Mat paddedSrc;
    cv::copyMakeBorder(src, paddedSrc, radius, radius, radius, radius, cv::BORDER_REFLECT);

    for (int y = radius; y < paddedSrc.rows - radius; ++y) {
        for (int x = radius; x < paddedSrc.cols - radius; ++x) {
            std::vector<uchar> values;
            for (int j = -radius; j <= radius; ++j) {
                for (int i = -radius; i <= radius; ++i) {
                    if (circularMask.at<uchar>(j + radius, i + radius) == 1)
                    {
                        values.push_back(paddedSrc.at<uchar>(y + j, x + i));
                    }
                }
            }
            std::sort(values.begin(), values.end());
            dst.at<uchar>(y - radius, x - radius) = values[values.size() / 2];
        }
    }
}
*/


void circularMedianFilterForBinaryImage(const cv::Mat& src, cv::Mat& dst, const cv::Mat& structuringElement)
{
    int radiush = (structuringElement.cols-1) / 2;
    int radiusv = (structuringElement.rows-1) / 2;
    dst = src.clone();
    cv::Mat paddedSrc;
    cv::copyMakeBorder(src, paddedSrc, radiusv, radiusv, radiush, radiush, cv::BORDER_REFLECT);
    int structuringElementArea = 0;
    for (int j = -radiush; j <= radiush; ++j) {
        for (int i = -radiusv; i <= radiusv; ++i) {
            if (structuringElement.at<uchar>(j + radiush, i + radiusv) == 1)
            {
                structuringElementArea++;
            }
        }
    }
    structuringElementArea /= 2;
    for (int y = radiusv; y < paddedSrc.rows - radiusv; ++y)
    {
        for (int x = radiush; x < paddedSrc.cols - radiush; ++x)
        {
            int count = 0;;
            for (int j = -radiush; j <= radiush; ++j)
            {
                for (int i = -radiusv; i <= radiusv; ++i)
                {
                    if (structuringElement.at<uchar>(j + radiush, i + radiusv) > 0)
                        if(paddedSrc.at<uchar>(y + j, x + i) > 0)
                            count++;
                }
            }
            if(count > structuringElementArea)
                dst.at<uchar>(y - radiusv, x - radiush) = 255;
            else
                dst.at<uchar>(y - radiusv, x - radiush) = 0;
        }
    }
}

void contoursCleanup(std::vector<cv::Vec4i>* hierarchy, std::vector<std::vector<cv::Point>>* contours)
{
    int cmax = contours->size();
    std::vector<int> indices;
    indices.resize(cmax);
    for(int c = 0; c < cmax; c++)
        indices[c] = c;
    for(int c = contours->size()-1; c >= 0; c--)
    {
        if((*contours)[c].size() < 3)
        {
            contours->erase(contours->begin()+c);
            hierarchy->erase(hierarchy->begin()+c);
            indices.erase(indices.begin()+c);
        }
        else
        {
            double area = cv::contourArea((*contours)[c]);
            if(area <= minimum_grain_area)
            {
                contours->erase(contours->begin()+c);
                hierarchy->erase(hierarchy->begin()+c);
                indices.erase(indices.begin()+c);
            }
        }
    }
    int cmax2 = contours->size();
    std::vector<int> indices2;
    indices2.resize(cmax, -1);
    for(int c = 0; c < cmax2; c++)
        indices2[indices[c]] = c;

    for(int c = 0; c < cmax2; c++)
    {
        for(int i = 0; i < 4; i++)
        {
            if((*hierarchy)[c][i] >= 0)
            {
               (*hierarchy)[c][i] = indices2[(*hierarchy)[c][i]];
            }
        }
    }
}

std::vector<std::vector<cv::Point>> getContours(cv::Mat src)
{
    cv::Mat binary;
    cv::Mat gray;
//    cv::cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(src, gray, cv::COLOR_RGB2GRAY);
    cv::threshold(gray, binary, grey_level_threshold, 255, thresholding_algorithm);

    cv::Mat strelem = cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                                cv::Size(2 * structuring_element_radius + 1,
                                                         2 * structuring_element_radius + 1),
                                                cv::Point(structuring_element_radius,
                                                          structuring_element_radius));

    std::vector<cv::Vec4i> hierarchy;
    std::vector<std::vector<cv::Point>> contours;

//    cv::erode(binary, gray, strelem);
//    cv::dilate(gray, binary, strelem);
//    cv::findContours(binary, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_NONE, cv::Point(0,0));

    circularMedianFilterForBinaryImage(binary, gray, strelem);
    cv::findContours(gray, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_NONE, cv::Point(0,0));

    contoursCleanup(&hierarchy, &contours);

    std::vector<cv::Vec4i> pairs = getAllPointPairs(contours, hierarchy);
    std::vector<std::vector<cv::Point>> untied = getUntiedContours(contours, pairs);

    int xmax = src.cols-1;
    int ymax = src.rows-1;
    for(int c = untied.size()-1; c >= 0; c--)
    {
        double area = -cv::contourArea(untied[c], true);
        if(area > maximum_grain_area || area <= minimum_grain_area)
        {
            untied.erase(untied.begin()+c);
        }
        int imax = untied[c].size();
        for(int i = 0; i < imax; i++)
        {
            if(untied[c][i].x == 0 || untied[c][i].x == xmax || untied[c][i].y == 0 || untied[c][i].y == ymax)
            {
                untied.erase(untied.begin()+c);
                break;
            }
        }
    }
    return untied;
}


/*

#include <iostream>
#include <vector>
#include <memory>
#include <stdexcept>

// Simple custom allocator
template <typename T>
class CustomAllocator {
public:
    using value_type = T;

    CustomAllocator() = default;

    template <typename U>
    constexpr CustomAllocator(const CustomAllocator<U>&) noexcept {}

    [[nodiscard]] T* allocate(std::size_t n) {
        if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
            throw std::bad_alloc();

        T* ptr = static_cast<T*>(std::malloc(n * sizeof(T)));
        if (!ptr)
            throw std::bad_alloc();

        return ptr;
    }

    void deallocate(T* p, std::size_t) noexcept {
        std::free(p);
    }
};

template <typename T, typename U>
bool operator==(const CustomAllocator<T>&, const CustomAllocator<U>&) { return true; }

template <typename T, typename U>
bool operator!=(const CustomAllocator<T>&, const CustomAllocator<U>&) { return false; }

int main() {
    // Use the custom allocator for a vector
    std::vector<int, CustomAllocator<int>> myVector;

    // Fill the vector with some values
    for (int i = 0; i < 10; ++i) {
        myVector.push_back(i * 10);
    }

    // Display the contents
    for (const auto& val : myVector) {
        std::cout << val << ' ';
    }
    std::cout << std::endl;

    return 0;
}


*/
