#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextDocument>
#include <QPrinter>
#include <QPainter>
#include <QItemDelegate>
#include <QtWidgets/QWidget>
#include <QtCharts/QChartGlobal>
#include <QJsonObject>

QT_CHARTS_BEGIN_NAMESPACE
class QChartView;
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

#include <opencv2/imgproc.hpp>
//#include <opencv2/imgproc/imgproc_c.h>
#include "serviceconfig.h"
#include "paramtreewidget.h"


namespace Ui {
class MainWindow;
}

typedef QList<double> DoubleList;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();



private slots:

    void tabCloseRequested(int index);
    void mousePointed(double x, double y);
    void zoomChanged(double zoom);

    void on_actionOpen_triggered();
    void on_actionSegment_triggered();
    void on_actionAbout_triggered();
    void on_actionExit_triggered();
    //void on_actionLoad_setup_triggered();
    void on_actionSave_triggered();
    void on_actionPrint_triggered();
    void on_tableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_actionRemove_item_triggered();
    
    void on_actionCompute_T_triggered();

    void on_actionScan_triggered();

    void on_actionTst_triggered();

    void on_actionService_configuration_triggered();

    void on_comboBox_segmentation_currentIndexChanged(int index);

    void on_actionClose_triggered();

    void on_actionSave_images_triggered();

    void on_actionLoad_images_triggered();

    void on_actionUpload_to_server_triggered();

    void on_pushButton_find_kernels_clicked();

    void on_pushButton_upload_clicked();

    void on_pushButton_save_images_clicked();

    void on_pushButton_save_report_clicked();

    void on_pushButton_print_clicked();

    void on_pushButton_remove_item_clicked();

    void on_pushButton_scan_clicked();

    void on_pushButton_close_all_clicked();

    void on_pushButton_compute_t_clicked();

    void on_doubleSpinBox_valueChanged(double arg1);

    void on_horizontalSlider_zoom_actionTriggered(int action);

    void on_pushButton_zoomout_clicked();

    void on_pushButton_zoomin_clicked();

    void on_pushButton_zoomorig_clicked();

    void on_tabWidget_images_currentChanged(int tab_index);

    void on_tableWidget_itemSelectionChanged();

    void on_doubleSpinBox_editingFinished();

    void on_radioButton_weight_toggled(bool checked);

    void on_actionSave_masks_triggered();

protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);

private:
    ParameterTreeWidget* tree;
    QJsonObject jsonObject;
    SftpConfig serviceConfig;
    QIcon icon_accepted;
    QIcon icon_attention;
    QIcon icon_attention_big;
    void jsonReadSegmentationOption(QJsonObject &json, const unsigned int selected);
    void jsonReadSegmentationOptions(const QJsonObject &json);
    void jsonWriteSegmentationOptions(QJsonObject &json);
    void jsonReadDataTabpageOptions(const QJsonObject &json);
    void jsonWriteDataTabpageOptions(QJsonObject &json);
    void jsonReadServiceOptions(const QJsonObject &json);
    void jsonWriteServiceOptions(QJsonObject &json);
    bool loadServiceOptions(const QString filename);
    bool saveServiceOptions(const QString filename);
    void rate();
    bool demoExpired();

    double t_value;
    int t_count;

    void makeCharts(void);
    void emptyReport(void);
    void appendImage(cv::Mat& image, const QString tabname, const bool imported);
    bool loadImage(const QString* fileName);
    void loadImages(const QString* fileName);


//    QPixmap makePixmap(CvSeq* contour, CvRect box, int decimate);
//    void clearRedraw(void);
//    CvSeq* getContourFromRow(int kernel_image, int row);

    void paintTreeData(QPainter* painter, QRect* linerect, QTreeWidget* treeWidget);
    void saveReport(const QString* fileName);
    //QTextDocument *createReport(void);
    void createReport(QPrinter* printer, bool printCharts, bool printTable);
    void saveImages(const QString* fileName);


    //bool ignore;

    Ui::MainWindow *ui;

//    std::vector<RyzImage> images;
//    IplImage* src;
//    //IplImage* dist;
//    IplImage* screen;
//    CvMemStorage* storage;
//    CvSeq* contours;
//    CvSeq* cr;

    struct SingleBar
    {
        double value;
        QString name;
    };

    struct SingleThreshold
    {
        int value;
        QString name;
    };

    struct
    {
         bool operator()(QPointF a, QPointF b) const
         {
             return a.x() < b.x();
         }
     } QPointF_xLess;


    double makeChartsData(QList<QPointF>* line, QList <SingleThreshold>* line_thresholds, QList <SingleBar>* bars_length, QList <SingleBar>* bars_roundness);
    double makeChartsDataArea(QList<QPointF>* line, QList <SingleThreshold>* line_thresholds, QList <SingleBar>* bars_length, QList <SingleBar>* bars_roundness);

    QChart* makeLineChart(QList<QPointF>* lista, QList<SingleThreshold> *thresholds, QString name);
    QChart* makeBarChart(QList <SingleBar>* lista, QString name, unsigned int color);



//    int counts_size_classes[6];
//    double areas_size_classes[6];
//    int counts_roundness_classes[4];
//    int areas_roundness_classes[4];




/*
Całe ziarno całe (whole kernel) – długość > 9/10 średniej długości całego ziarna
Ryż cały (head rice)  – ziarno całe lub część >=  ¾ długości średniej całego ziarna
Ziarno nadłamane (large broken kernel) – długość <niż ¾ ale większa niż ½ średniej długości całego ziarna
Ziarno połamane (midium broken kernel) – długość jest =<niż ½ ale większa niż ¼ średniej długości całego ziarna
Okruchy ziarna (small broken kernel) części ziarna długość =<niż ¼ średniej długości całego ziarna
Pył (chip) ziarno poniżej 1.4 mm
*/
    #define kernel_size_count 5
    const QString kernel_size_names[kernel_size_count] =
    {
        tr("small broken kernel"),
        tr("medium broken kernel"),
        tr("large broken kernel"),
        tr("head rice"),
        tr("whole kernel")
    };
    const double kernel_size_thresholds[kernel_size_count] = {0.25, 0.50, 0.75, 0.90, 100000000.0};


/*
Smukły (Slender)  długość / szerokości)*100% = powyżej 3,0
Średni (medium) długość / szerokości)*100% = powyżej 2,1-3,0
Pogrubiony (bold) długość / szerokości)*100% = powyżej 1,1-2,0
Ookrągły (round) długość / szerokości)*100% = powyżej 1,0 i mniej
*/
    const QString kernel_roundness_names[4] =
    {
        tr("round"),
        tr("bold"),
        tr("medium"),
        tr("slender")
    };
    const double kernel_roundness_thresholds[4] = {1.05, 2.05, 3.0, 100000000.0};
    double rnd(double in);



//    ItemDelegate* idelegate;
};

#endif // MAINWINDOW_H
