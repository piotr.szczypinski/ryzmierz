#include "ryzrender.h"
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QApplication>
#include <QScrollArea>
#include <QScrollBar>

RyzRender::RyzRender(QWidget *parent) : QWidget(parent)
{
    screen_zoom = 1.0;
//    image = NULL;
//    screen = NULL;
//    contours = NULL;
//    contourCount = -1;
    qApp->installEventFilter(this);
}

RyzRender::~RyzRender()
{
}

QPixmap RyzRender::getPixmap(const std::vector<cv::Point>& cr, const int decimate)
{
    QPixmap scaledpixmap;
    cv::Rect box = cv::boundingRect(cr);
    int decim;
    bool fast;
    if(decimate > 0)
    {
        decim = decimate;
        fast = false;
    }
    else if(decimate < 0)
    {
        decim = -decimate;
        fast = true;
    }
    else
        return scaledpixmap;

    unsigned int height = box.height/decim;
    unsigned int width = box.width/decim;

    if(width <= 0 || height <= 0)
        return scaledpixmap;

    unsigned int strade = width*4;
    unsigned char* image_data = new unsigned char[strade*height];

    int minY = box.y+(box.height%(height*decim))/2;
    int minX = box.x+(box.width%(width*decim))/2;

    if(fast)
    {
        for(int y = 0; y < height; y++)
        {
            unsigned char* sr = ((unsigned char *)image.data + (minY + y*decim) * image.step) + minX*3;
            unsigned char* ds = (image_data + y * strade);

            for(int x = 0; x < width; x++)
            {
                ds[0] = sr[2];
                ds[1] = sr[1];
                ds[2] = sr[0];
                ds[3] = 255;
                sr += (3*decim);
                ds += 4;
            }
        }
    }
    else
    {
        for(int y = 0; y < height; y++)
        {
            unsigned char* sr = ((unsigned char *)image.data + (minY + y*decim) * image.step) + minX*3;
            unsigned char* ds = (image_data + y * strade);

            for(int x = 0; x < width; x++)
            {
                float inside = cv::pointPolygonTest(cr, cv::Point2f(minX + x * decim, minY + y * decim), true);
                //float inside = *in;// * ink;
                if(inside >= decim*2)
                {
                    ds[0] = sr[2];
                    ds[1] = sr[1];
                    ds[2] = sr[0];
                    ds[3] = 255;
                }
                else if(inside >= 0)
                {
                    ds[0] = sr[2];
                    ds[1] = sr[1];
                    ds[2] = sr[0];
                    ds[3] = inside*127/decim;
                }
                else
                {
                    ds[0] = 255;
                    ds[1] = 255;
                    ds[2] = 255;
                    ds[3] = 0;
                }
                sr += (3*decim);
                ds += 4;
                //in += decimate;
            }
        }
    }

    QImage qimage = QImage((uchar*)image_data, width, height, QImage::Format_ARGB32);//.rgbSwapped();
    scaledpixmap = QPixmap(QPixmap::fromImage(qimage));
    delete[] image_data;
    return scaledpixmap;
}


QPixmap RyzRender::getPixmap(const int i, const int decimate)
{
    QPixmap scaledpixmap;
    int cmax = contours.size();
    if(i < cmax)
        return getPixmap(contours[i], decimate);
    return scaledpixmap;
}


int RyzRender::getContourIndex(const double x, const double y)
{
    int cmax = contours.size();
    for(int c = 0; c < cmax; c++)
    {
        cv::Point ppi = contours[c][0];
        if(abs(ppi.x-(int)x) < max_contour_distance && abs(ppi.y-(int)y) < max_contour_distance)
        {
            double inside = cv::pointPolygonTest(contours[c], cv::Point2f(x, y), 0);
            if(inside >= 0)
            {
                return c;
            }
        }
    }
    return -1;
}


void RyzRender::setImage(cv::Mat& src)
{
    image = src;
    repaintContour(-1);
}

void RyzRender::setContours(std::vector<std::vector<cv::Point> > &contours)
{
    this->contours = contours;
    max_contour_distance = 0;
    int cmax = contours.size();
    for(int c = 0; c < cmax; c++)
    {
       cv::Rect box = cv::boundingRect(contours[c]);
       if(box.height > max_contour_distance)
           max_contour_distance = box.height;
       if(box.width > max_contour_distance)
           max_contour_distance = box.width;
    }
    repaintContour(-1);
}

void RyzRender::repaintContour(const int i)
{
    if(image.empty())
        return;
    image.copyTo(screen);

    int cmax = contours.size();

    for(int c = 0; c < cmax; c++)
    {
        if(i == c)
        {
            cv::drawContours(screen, contours, c, cv::Scalar(0, 63, 255), 3);
            cv::Rect box = cv::boundingRect(contours[c]);
            centerAt(box.x+box.width/2, box.y+box.height/2);
        }
        else
            cv::drawContours(screen, contours, c, cv::Scalar(255, 0, 0), 1);
    }
    screen_image = QImage((uchar*)screen.data, screen.cols, screen.rows, screen.step[0], QImage::Format_RGB888);//.rgbSwapped();
    if(! screen_image.isNull())
    {
        resize(screen_zoom * screen_image.size());
    }
    update();
}

void RyzRender::setZoom(double zoom)
{
    QWidget* papa = parentWidget();
    if(papa != NULL)
    {
        papa = papa->parentWidget();
        if(papa != NULL)
        {
            QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
            if(pasa != NULL)
            {
                int x, y;
                QScrollBar* sbh = pasa->horizontalScrollBar();
                QScrollBar* sbv = pasa->verticalScrollBar();
                if(sbh != NULL)
                {
                    int sw = sbh->value();
                    x = (sw + (width() - sbh->maximum())/2);
                    x = sw + (x/screen_zoom - x/zoom)*zoom;
                }
                if(sbh != NULL)
                {
                    int sh = sbv->value();
                    y = (sh + (height() - sbv->maximum())/2);
                    y = sh + (y/screen_zoom - y/zoom)*zoom;
                }
                resize(zoom * screen_image.size());
                if(sbh != NULL)
                    sbh->setValue(x);
                if(sbh != NULL)
                    sbv->setValue(y);
                screen_zoom = zoom;
                //update();
            }
        }
    }
}

void RyzRender::paintEvent(QPaintEvent *e)
{
    if(screen_image.isNull())
        return;
    QPainter painter(this);
    if(screen_zoom == 1.0)
    {
        painter.drawImage(screen_image.rect(), screen_image);
    }
    else
    {
        painter.drawImage(contentsRect(), screen_image);
    }
    e->accept();
}

void RyzRender::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        emit mousePointed((double) ev->x() / screen_zoom, (double) ev->y() / screen_zoom);
    }
}

void RyzRender::mouseReleaseEvent(QMouseEvent *ev)
{
    ev->accept();
    emit mouseReleased();
}


void RyzRender::zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom)
{
    if(screen_image.isNull())
        return;
    QWidget* papa = parentWidget();
    if(papa == NULL)
        return;
    papa = papa->parentWidget();
    if(papa == NULL)
        return;
    QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
    if(pasa == NULL)
        return;
    QScrollBar* sb = pasa->horizontalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (x/previous_zoom - x/current_zoom)*current_zoom);
    sb = pasa->verticalScrollBar();
    if(sb != NULL)
        sb->setValue(sb->value() + (y/previous_zoom - y/current_zoom)*current_zoom);
}

void RyzRender::centerAt(int x, int y)
{
    if(screen_image.isNull())
        return;
    QWidget* papa = parentWidget();
    if(papa == NULL)
        return;
    papa = papa->parentWidget();
    if(papa == NULL)
        return;
    QScrollArea* pasa = qobject_cast<QScrollArea*>(papa);
    if(pasa == NULL)
        return;

//    int xd = x * screen_zoom;
//    int yd = y * screen_zoom;
    QSize size = screen_image.size();

    QScrollBar* sb = pasa->horizontalScrollBar();
    if(sb != NULL)
        sb->setValue(screen_zoom*(x - size.width()/2) + sb->maximum()/2);
    sb = pasa->verticalScrollBar();
    if(sb != NULL)
        sb->setValue(screen_zoom*(y - size.height()/2) + sb->maximum()/2);

}

void RyzRender::wheelEvent(QWheelEvent *ev)
{
    if(QApplication::keyboardModifiers() & Qt::ControlModifier)
    {
        double previous_zoom = screen_zoom;
        ev->accept();
        if(ev->angleDelta().y() > 0)
        {
            if(screen_zoom < 10)
            {
                screen_zoom *= 1.2;
                if(screen_zoom > 0.9 && screen_zoom < 1.1)
                    screen_zoom = 1.0;
                if(! screen_image.isNull())
                {
                    resize(screen_zoom * screen_image.size());
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, screen_zoom);
                }
                emit zoomChanged(screen_zoom);
                //update();
            }
        }
        else if(ev->angleDelta().y() < 0)
        {
            if(screen_zoom > 0.1)
            {
                screen_zoom /= 1.2;
                if(screen_zoom > 0.9 && screen_zoom < 1.1)
                    screen_zoom = 1.0;
                if(! screen_image.isNull())
                {
                    zoomKeepLocation(ev->position().x(), ev->position().y(), previous_zoom, screen_zoom);
                    resize(screen_zoom * screen_image.size());
                }
                emit zoomChanged(screen_zoom);
                //update();
            }
        }
    }
}
