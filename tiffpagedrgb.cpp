#include "tiffpagedrgb.h"

#define RYZ_DOC_NAME "RYZ2019"
#define RYZ_DOC_IMPO "RYZXXXX"

void TiffPageWriter(TIFF* tiff, const cv::Mat& image, const std::string* info, bool imported)
{
    unsigned int width = image.cols;
    unsigned int stride = image.step;
    unsigned int height = image.rows;
    unsigned char* data = (unsigned char*) image.data;

    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 3);
    //TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, height);
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    TIFFSetField(tiff, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
    TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

    TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)600);
    TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)600);
    TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);

    TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, info->c_str());

    if(imported)
        TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, RYZ_DOC_IMPO);
    else
        TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, RYZ_DOC_NAME);

    for(unsigned int y = 0; y < height; y++)
    {
        TIFFWriteScanline(tiff, (void*) data, y, 0);
        data += stride;
    }
    TIFFWriteDirectory(tiff);
}

int PagedTiffWriter(const char *filename, const std::vector<cv::Mat>& images, const std::vector <std::string>* infos, std::vector<bool> *impl)
{
    unsigned int page;
    unsigned int maxpage = images.size();
    if(maxpage <= 0)
        return 1;
    if(maxpage != infos->size())
        return 2;
    TIFF* tiff = TIFFOpen(filename, "w");
    if (!tiff)
        return 3;

    for(page = 0; page < maxpage; page++)
    {
        TiffPageWriter(tiff, images[page], &(*infos)[page], (*impl)[page]);
    }
    TIFFClose(tiff);
    return 0;
}

cv::Mat TiffPageReader(TIFF* tiff, std::string* info, bool* imported)
{
    cv::Mat return_result;
    char* name;
    uint32_t width;
    uint32_t height;
    //uint32 temp32;
    uint16_t temp16;
    float tempf;
    *imported = false;

    if(TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &width) != 1)
        return return_result;
    if(TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &height) != 1)
        return return_result;
    TIFFGetFieldDefaulted(tiff, TIFFTAG_BITSPERSAMPLE, &temp16); 
    if(temp16 != 8)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_SAMPLESPERPIXEL, &temp16);
    if(temp16 != 3)
        return return_result;
//    TIFFGetField(tiff, TIFFTAG_ROWSPERSTRIP, &temp32);
//    if(temp32 != height)
//        return 0x12;
    TIFFGetField(tiff, TIFFTAG_COMPRESSION, &temp16);
    if(temp16 != COMPRESSION_NONE)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_PHOTOMETRIC, &temp16);
    if(temp16 !=  PHOTOMETRIC_RGB)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_FILLORDER, &temp16);
    if(temp16 !=  FILLORDER_MSB2LSB)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_PLANARCONFIG, &temp16);
    if(temp16 !=  PLANARCONFIG_CONTIG)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_XRESOLUTION, &tempf);
    if(tempf != (float)600)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_YRESOLUTION, &tempf);
    if(tempf != (float)600)
        return return_result;
    TIFFGetField(tiff, TIFFTAG_RESOLUTIONUNIT, &temp16);
    if(temp16 != RESUNIT_INCH)
        return return_result;
    
    name = NULL;
    if (TIFFGetField(tiff, TIFFTAG_IMAGEDESCRIPTION, &name) == 1 && name != NULL)
    {
        *info = name;
    }
    else
    {
        return return_result;
    }
    
    if (TIFFGetField(tiff, TIFFTAG_DOCUMENTNAME, &name) == 1 && name != NULL)
    {
        if(strcmp(RYZ_DOC_NAME, name) != 0)
            *imported = true;
    }
    else
    {
        return return_result;
    }
    cv::Mat image(height, width, CV_8UC3);
    if(image.empty())
    {
        return return_result;
    }
    unsigned char* buf = (unsigned char*) image.data;
    if(buf == NULL)
    {
        return return_result;
    }
    int stride = image.step;
    memset(buf, 0, (size_t)height*stride);

    tmsize_t lines = TIFFScanlineSize(tiff);
    if(lines > stride)
    {
        return return_result;
    }
    for(unsigned int y = 0; y < height; y++)
    {
        if(TIFFReadScanline(tiff, (void*)buf, y) != 1)
            break;
        buf += stride;
    }
    return return_result;
}

std::vector<cv::Mat> PagedTiffReader(const char *filename, std::vector <std::string>* infos, std::vector<bool> *impl)
{
    std::vector<cv::Mat> return_result;
    TIFF* tiff = TIFFOpen(filename, "r");
    if (!tiff)
        return return_result;
    uint16_t maxpages = TIFFNumberOfDirectories(tiff);
    if(maxpages <= 0)
        return return_result;

    unsigned int page = 0;
    do
    {
        std::string info;
        bool imp;
        cv::Mat image = TiffPageReader(tiff, &info, &imp);
        if(image.empty())
            break;
        return_result.push_back(image);
        infos->push_back(info);
        impl->push_back(imp);
        page++;
    }
    while(TIFFReadDirectory(tiff) && page < maxpages);
    TIFFClose(tiff);
    return return_result;
}
