#ifndef TIFFPAGEDRGB_H
#define TIFFPAGEDRGB_H

#include <vector>
#include <string>
#include "tiffio.h"
#include <opencv2/imgproc.hpp>

//#include <opencv2/imgproc/imgproc_c.h>
//#include <opencv2/imgproc/imgproc.hpp>

int PagedTiffWriter(const char *filename,
                    const std::vector<cv::Mat>& images,
                    const std::vector <std::string>* infos,
                    std::vector<bool> *impl);
std::vector<cv::Mat> PagedTiffReader(const char *filename,
                    std::vector <std::string>* infos,
                    std::vector <bool>* impl);

#endif // TIFFPAGEDRGB_H
