#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "filenameremap.h"
//#include "reportwriter.h"

#include <string>
#include <sstream>

//#include <opencv2/highgui/highgui_c.h>
//#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <QFileDialog>
#include <QMessageBox>
#include <QDropEvent>
#include <QMimeData>
#include <QDesktopServices>

#include <QPdfWriter>
#include <QPainter>
#include <QPrintDialog>

#include <QTextCursor>
#include <QTextTable>
#include <QTextDocumentWriter>
#include <QTableWidgetItem>
#include <QDateTime>

#include <QtCharts/QChartView>
#include <QtCharts/QPercentBarSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QValueAxis>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QBarCategoryAxis>

#include "ryzrender.h"
#include "ryzjson.h"
#include "ryzcontour.h"
#include "scanner.h"
#include "tiffpagedrgb.h"



extern unsigned int decimate_for_icon;
extern double image_dpi;

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>


#define ELEMENT_KOLUMNY_1 ui->tableWidget->item(c, 1)->text()
#define ELEMENT_KOLUMNY_2 ui->tableWidget->item(c, 2)->text()
#define ELEMENT_KOLUMNY_3 ui->tableWidget->item(c, 3)->text()
#define ELEMENT_NAGLOWKA_0 ui->tableWidget->horizontalHeaderItem(0)->text()
#define ELEMENT_NAGLOWKA_1 ui->tableWidget->horizontalHeaderItem(1)->text()
#define ELEMENT_NAGLOWKA_2 ui->tableWidget->horizontalHeaderItem(2)->text()
#define ELEMENT_NAGLOWKA_3 ui->tableWidget->horizontalHeaderItem(3)->text()

#define ELEMENT_KOLUMNY_LENGTH ui->tableWidget->item(r, 1)->data(Qt::DisplayRole).toDouble()
#define ELEMENT_KOLUMNY_AREA ui->tableWidget->item(r, 3)->data(Qt::DisplayRole).toDouble()
#define ELEMENT_KOLUMNY_ROUNDNESS ui->tableWidget->item(r, 4)->data(Qt::DisplayRole).toDouble()

// Oxygen Icon Theme has been developed by The Oxygen Team.
//ref: https://github.com/pasnox/oxygen-icons-png
//ref: https://stackoverflow.com/questions/50156082/changing-text-color-for-each-label-in-qcategoryaxis





void MainWindow::on_actionTst_triggered()
{
    QString fni("/home/piotr/Program/Ryz/EpsonV370-ryz600dpi-bialy1.png");
    loadImage(&fni);
    ui->doubleSpinBox->setValue(6.543210);
    on_actionSegment_triggered();
//    rate();
    QString fn("test.pdf");
    saveReport(&fn);
}






void MainWindow::jsonReadSegmentationOption(QJsonObject &json, const unsigned int selected)
{
    if(json.contains("segmentation_options"))
    {
        QJsonArray segmentation_options = json["segmentation_options"].toArray();
        if((int) selected < segmentation_options.size())
        {
            QJsonObject segmentation_options_object = segmentation_options[selected].toObject();
            ryzJsonRead(segmentation_options_object);
        }
    }
}

void MainWindow::on_comboBox_segmentation_currentIndexChanged(int index)
{
    jsonReadSegmentationOption(jsonObject, index);
}

void MainWindow::jsonWriteDataTabpageOptions(QJsonObject &json)
{
    if(jsonObject.contains("data_tabpage"))
    {
        json["data_tabpage"] = jsonObject["data_tabpage"];
    }
    else
    {
        QJsonArray data_tab_lines;
        {
            QJsonObject data_tab_line;
            data_tab_line["property"] = "User name";
            data_tab_line["template"] = "John Doe";
            data_tab_line["tip"] = "Enter user name or ID";
            data_tab_lines.append(data_tab_line);
            data_tab_line["property"] = "Current date";
            data_tab_line["template"] = "p;?";
            data_tab_line["tip"] = "Current date is not editable";
            data_tab_lines.append(data_tab_line);
        }
        QJsonObject jsont;
        jsont["data_tabpage_lines"] = data_tab_lines;
        json["data_tabpage"] = jsont;
    }
}

void MainWindow::jsonReadDataTabpageOptions(const QJsonObject &json)
{
    tree = ui->treeWidget;
    tree->clear();
    if(json.contains("data_tabpage_lines"))
    {
        QTreeWidgetItem* item = NULL;
        QJsonArray data_tab_lines = json["data_tabpage_lines"].toArray();
        for (int i = 0; i < data_tab_lines.size(); ++i)
        {
            QJsonObject data_tab_line = data_tab_lines[i].toObject();
            QString property = data_tab_line["property"].toString();
            QString value = data_tab_line["template"].toString();
            QString tip = data_tab_line["tip"].toString();
            tree->createProperty(item, property, value, tip);
        }
        tree->expandAll();
    }
}

double tlx = -1;
double tly = -1;
double brx = -1;
double bry = -1;
int orientation = -1;


void MainWindow::jsonReadServiceOptions(const QJsonObject &json)
{
    if(json.contains("data_tabpage"))
    {
        QJsonObject data_tabpage = json["data_tabpage"].toObject();
        jsonReadDataTabpageOptions(data_tabpage);
    }
    if(json.contains("sftp_options"))
    {
        QJsonObject sftp_options = json["sftp_options"].toObject();
        sftpJsonReadSftpOptions(sftp_options, &serviceConfig);
        unscramblePassword(&serviceConfig);
    }


    if(json.contains("scanner_options"))
    {
        QJsonObject options = json["scanner_options"].toObject();
        if(options.contains("tlx"))
            tlx = options["tlx"].toDouble();
        if(options.contains("tly"))
            tly = options["tly"].toDouble();
        if(options.contains("brx"))
            brx = options["brx"].toDouble();
        if(options.contains("bry"))
            bry = options["bry"].toDouble();
        if(options.contains("orientation"))
            orientation = options["orientation"].toInt();
    }


    if(json.contains("segmentation_options"))
    {
        QJsonArray segmentation_options = json["segmentation_options"].toArray();
        for (int i = 0; i < segmentation_options.size(); ++i)
        {
            QJsonObject jsonelem = segmentation_options[i].toObject();
            if(jsonelem.contains("segmentation_option_name"))
            {
                ui->comboBox_segmentation->addItem(jsonelem["segmentation_option_name"].toString());
            }
            else
                ui->comboBox_segmentation->addItem("unknown");
        }
    }
}

void MainWindow::jsonWriteServiceOptions(QJsonObject &json)
{
    jsonWriteDataTabpageOptions(json);

    QJsonObject sftp_options;
    SftpConfig config = serviceConfig;
    scramblePassword(&config);
    sftpJsonWriteSftpOptions(sftp_options, &config);
    json["sftp_options"] = sftp_options;

    if(json.contains("segmentation_options"))
    {
        QJsonArray segmentation_options = json["segmentation_options"].toArray();
        json["segmentation_options"] = segmentation_options;
    }
    else
    {
        if(jsonObject.contains("segmentation_options"))
        {
            json["segmentation_options"] = jsonObject["segmentation_options"];
        }
        else
        {
            QJsonArray segmentation_options;
            {
                QJsonObject jsonopt;
                jsonopt["segmentation_option_name"] = "default";
                ryzJsonWrite(jsonopt);
                segmentation_options.append(jsonopt);
            }
            json["segmentation_options"] = segmentation_options;
        }
    }
}

bool MainWindow::loadServiceOptions(const QString filename)
{
    QFile loadFile(filename);
    if (!loadFile.open(QIODevice::ReadOnly))
        return false;
    QByteArray jsonData = loadFile.readAll();
    QJsonDocument jsonDoc(QJsonDocument::fromJson(jsonData));
    jsonReadServiceOptions(jsonDoc.object());
    jsonObject = jsonDoc.object();
    return true;
}

bool MainWindow::saveServiceOptions(const QString filename)
{
    QFile saveFile(filename);
    if (!saveFile.open(QIODevice::WriteOnly))
        return false;
    QJsonObject object;
    jsonWriteServiceOptions(object);
    QJsonDocument saveDoc(object);
    saveFile.write(saveDoc.toJson());
    return true;
}



//#define BUILDTM_YEAR (\
//    __DATE__[7] == '?' ? 1900 \
//    : (((__DATE__[7] - '0') * 1000 ) \
//    + (__DATE__[8] - '0') * 100 \
//    + (__DATE__[9] - '0') * 10 \
//    + __DATE__[10] - '0'))
//#define BUILDTM_MONTH (\
//    __DATE__ [2] == '?' ? 1 \
//    : __DATE__ [2] == 'n' ? (__DATE__ [1] == 'a' ? 1 : 6) \
//    : __DATE__ [2] == 'b' ? 2 \
//    : __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ? 3 : 4) \
//    : __DATE__ [2] == 'y' ? 5 \
//    : __DATE__ [2] == 'l' ? 7 \
//    : __DATE__ [2] == 'g' ? 8 \
//    : __DATE__ [2] == 'p' ? 9 \
//    : __DATE__ [2] == 't' ? 10 \
//    : __DATE__ [2] == 'v' ? 11 \
//    : 12)
//#define BUILDTM_DAY (\
//    __DATE__[4] == '?' ? 1 \
//    : ((__DATE__[4] == ' ' ? 0 : \
//    ((__DATE__[4] - '0') * 10)) + __DATE__[5] - '0'))
//#define BUILDTM_HOUR (\
//    __TIME__[0] == '?' ? 1 \
//    : ((__TIME__[0] == ' ' ? 0 : \
//    ((__TIME__[0] - '0') * 10)) + __TIME__[1] - '0'))
//#define BUILDTM_MINUTE (\
//    __TIME__[3] == '?' ? 1 \
//    : ((__TIME__[3] == ' ' ? 3 : \
//    ((__TIME__[3] - '0') * 10)) + __TIME__[4] - '0'))
//#define BUILDTM_SECOND (\
//    __TIME__[6] == '?' ? 1 \
//    : ((__TIME__[6] == ' ' ? 0 : \
//    ((__TIME__[6] - '0') * 10)) + __TIME__[7] - '0'))


#include <ctime>
bool MainWindow::demoExpired()
{
//    std::time_t t = std::time(nullptr);   // get time now

//    std::time_t rawtime;
//    std::tm * timeinfo;
//    std::time ( &rawtime );
//    timeinfo = localtime ( &rawtime );
//    timeinfo->tm_year = BUILDTM_YEAR - 1900;
//    timeinfo->tm_mon = BUILDTM_MONTH - 1;
//    timeinfo->tm_mday = BUILDTM_DAY;
//    timeinfo->tm_hour = BUILDTM_HOUR;
//    timeinfo->tm_min = BUILDTM_MINUTE;
//    timeinfo->tm_sec = BUILDTM_SECOND;

//    std::time_t tb = mktime ( timeinfo );
////    tb += 7948800;
////+7948800 trzy miesiące
//    tb += 8000000;
//    if(t > tb)
//    {
//        QMessageBox::about(this, tr("Expired version"), tr("This version of RyżMierz have expired and requires an update"));
//        return true;
//    }
    return false;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->horizontalSlider_zoom->setStyleSheet(
                "QSlider::groove:horizontal {"
                "background: #82BAD5;"
                "border: 1px solid #CCCECE;"
                "height: 2px;"
                "margin: 5 5 5 5;"
                "}"
                "QSlider::handle:horizontal {"
                "background: #82BAD5;"
                "width: 7px;"
                "margin: -4px 0 -4px 0;"
                "border: 1px solid #77839C;"
                "border-radius: 4px;"
                "}");


    if(demoExpired())
    {
        close();
        exit(0);
        return;
    }



    QFontMetrics m (ui->plainTextEdit_comments->font());
    int RowHeight = m.lineSpacing() ;
    ui->plainTextEdit_comments->setFixedHeight  (6 * RowHeight) ;
    //ignore = false;
    t_value = ui->doubleSpinBox->value();
    t_count = 0;
    connect(ui->tabWidget_images, SIGNAL(tabCloseRequested(int)), this, SLOT(tabCloseRequested(int)));
    ui->actionScan->setEnabled(scannerInit());

    icon_accepted.addFile(":/icons/task-accepted.png");
    icon_attention.addFile(":/icons/task-attention.png");
    icon_attention_big.addFile(":/icons/attention.png");
    loadServiceOptions("ryzOptions.json");
    unscramblePassword(&serviceConfig);
    jsonReadSegmentationOption(jsonObject, ui->comboBox_segmentation->currentIndex());
    makeCharts();
    //printf("password = %s\n", serviceConfig.password.toStdString().c_str());
}

MainWindow::~MainWindow()
{
    scannerFinit();
    delete ui;
}

void MainWindow::emptyReport(void)
{
    ui->tableWidget->setRowCount(0);
    ui->treeWidget_results->clear();
    makeCharts();
    ui->label_reference_state->setPixmap(icon_attention.pixmap(16));
    if(t_count > 0) t_count = -t_count;
}

double MainWindow::makeChartsData(QList<QPointF>* line, QList<SingleThreshold> *line_thresholds, QList <SingleBar>* bars_length, QList <SingleBar>* bars_roundness)
{
    double uniformity = 0;
    int i;
    int p;
    int rc = ui->tableWidget->rowCount();
    if(rc <= 0)
        return uniformity;

    DoubleList lengths;
    for(int r = 0; r < rc; r++)
    {
        lengths.append(ELEMENT_KOLUMNY_LENGTH);
    }
    std::sort(lengths.begin(), lengths.end());
    int lengths_count = lengths.count();
    if(lengths_count < 1)
        return uniformity;
    int thresholds_count = kernel_size_count;
    double kernel_size_thresholds_t[kernel_size_count];

    for(i = 0; i < thresholds_count; i++)
    {
        kernel_size_thresholds_t[i] = t_value*kernel_size_thresholds[i];
    }
    i = 0;
    p = 0;
    thresholds_count--;
    int counti;
    for(; p < thresholds_count; p++)
    {
        counti = 0;
        for(; i < lengths_count; i++)
        {
            if(lengths[i] < kernel_size_thresholds_t[p])
            {
                uniformity += lengths[i];
                line->append(QPointF(i+1, lengths[i]));
                counti++;
            }
            else
                break;
        }
        //countp += counti;
        line_thresholds->append({i, kernel_size_names[p]});
        bars_length->append({(double)counti*100.0/lengths_count, kernel_size_names[p]});
    }
    counti = 0;
    for(; i < lengths_count; i++)
    {
        uniformity += lengths[i];
        line->append(QPointF(i+1, lengths[i]));
        counti++;
    }
    line_thresholds->append({i, kernel_size_names[p]});
    bars_length->append({(double)counti*100.0/lengths_count, kernel_size_names[p]});

    uniformity /= (lengths_count*lengths[lengths_count-1]);

    lengths.clear();
    for(int r = 0; r < rc; r++)
    {
        lengths.append(ELEMENT_KOLUMNY_ROUNDNESS);
    }
    std::sort(lengths.begin(), lengths.end());
    lengths_count = lengths.count();
    if(lengths_count < 1)
        return uniformity;
    int rthresholds_count = sizeof(kernel_roundness_thresholds)/sizeof(kernel_roundness_thresholds[0]);
    i = 0;
    p = 0;
    rthresholds_count--;
    for(; p < rthresholds_count; p++)
    {
        counti = 0;
        for(; i < lengths_count; i++)
        {
            if(lengths[i] < kernel_roundness_thresholds[p])
                counti++;
            else
                break;
        }
        bars_roundness->append({(double)counti*100.0/lengths_count, kernel_roundness_names[p]});
    }
    counti = 0;
    for(; i < lengths_count; i++)
    {
            counti++;
    }
    bars_roundness->append({(double)counti*100.0/lengths_count, kernel_roundness_names[p]});
    return uniformity;
}

double MainWindow::makeChartsDataArea(QList<QPointF>* line, QList<SingleThreshold> *line_thresholds, QList <SingleBar>* bars_length, QList <SingleBar>* bars_roundness)
{
    double uniformity = 0;
    int i;
    int p;
    int rc = ui->tableWidget->rowCount();
    if(rc <= 0)
        return uniformity;

    double whole_area = 0.0;
    QList <QPointF> lengths;
    for(int r = 0; r < rc; r++)
    {
        double a = ELEMENT_KOLUMNY_AREA;
        whole_area += a;
        lengths.append({ELEMENT_KOLUMNY_LENGTH, a});
    }
    whole_area /= 100.0;

    std::sort(lengths.begin(), lengths.end(), QPointF_xLess);
    int lengths_count = lengths.count();
    if(lengths_count < 1)
        return uniformity;
    int thresholds_count = kernel_size_count;
    double kernel_size_thresholds_t[kernel_size_count];

    for(i = 0; i < thresholds_count; i++)
    {
        kernel_size_thresholds_t[i] = t_value*kernel_size_thresholds[i];
    }

    i = 0;
    p = 0;
    double area = 0.0;
    thresholds_count--;
    double areai = 0.0;
    for(; p < thresholds_count; p++)
    {
        for(; i < lengths_count; i++)
        {
            if(lengths[i].x() < kernel_size_thresholds_t[p])
            {
                double l = lengths[i].x();
                double a = lengths[i].y();
                area += a;
                line->append(QPointF(area/whole_area, lengths[i].x()));
                areai += a;
                uniformity += (l*a);
            }
            else
                break;
        }
        //countp += counti;
        line_thresholds->append({i, kernel_size_names[p]});
        bars_length->append({areai/whole_area, kernel_size_names[p]});
        areai = 0.0;
    }

    for(; i < lengths_count; i++)
    {
        double l = lengths[i].x();
        double a = lengths[i].y();
        area += a;
        line->append(QPointF(area/whole_area, lengths[i].x()));
        areai += a;
        uniformity += (l*a);
    }
    line_thresholds->append({i, kernel_size_names[p]});
    bars_length->append({areai/whole_area, kernel_size_names[p]});

    uniformity /= (area*lengths[lengths_count-1].x());

    lengths.clear();
    for(int r = 0; r < rc; r++)
    {
        lengths.append({ELEMENT_KOLUMNY_ROUNDNESS, ELEMENT_KOLUMNY_AREA});
    }
    std::sort(lengths.begin(), lengths.end(), QPointF_xLess);
    lengths_count = lengths.count();
    if(lengths_count < 1)
        return uniformity;
    int rthresholds_count = sizeof(kernel_roundness_thresholds)/sizeof(kernel_roundness_thresholds[0]);
    i = 0;
    p = 0;
    rthresholds_count--;
    for(; p < rthresholds_count; p++)
    {
        areai = 0.0;
        for(; i < lengths_count; i++)
        {
            if(lengths[i].x() < kernel_roundness_thresholds[p])
            {
                areai += lengths[i].y();
            }
            else
                break;
        }
        bars_roundness->append({areai/whole_area, kernel_roundness_names[p]});
    }
    areai = (100.0*whole_area - areai);
    bars_roundness->append({areai/whole_area, kernel_roundness_names[p]});
    return uniformity;
}


void MainWindow::makeCharts(void)
{
    QList<QPointF> line;
    QList <SingleThreshold> line_thresholds;
    QList <SingleBar> bars_length;
    QList <SingleBar> bars_roundness;


    //Piaskowy
    //0xffccaa
    //0xe5df87
    //0xf1deb6
    //Fiolet
    //0xc787e5
    //0xdfb6f1
    //Turkus
    //87dfe5
    //b6f1d6 *
    unsigned int color1;
    unsigned int color2;
    double uniformity = 0.0;
    if(ui->radioButton_weight->isChecked())
    {
        uniformity = makeChartsDataArea(&line, &line_thresholds, &bars_length, &bars_roundness);
        color1 = 0xb4ffd2;
        color2 = 0x9fe1ba;
    }
    else
    {
        uniformity = makeChartsData(&line, &line_thresholds, &bars_length, &bars_roundness);
        color1 = 0xffebc1;
        color2 = 0xe1cfaa;
    }

    QChart* ch0 = makeLineChart(&line, &line_thresholds, (tr("Length uniformity %1%")).arg(uniformity*100, 4, 'f', 2, '0' ));
    QChart* ch1 = makeBarChart(&bars_length, tr("Length classes"), color1);
    QChart* ch2 = makeBarChart(&bars_roundness, tr("Roundness classes"), color2);

    ui->gridLayout->addWidget(new QChartView(ch0), 1, 0);
    ui->gridLayout->addWidget(new QChartView(ch1), 2, 0);
    ui->gridLayout->addWidget(new QChartView(ch2), 3, 0);



    QTreeWidgetItem* topitem;
    ui->treeWidget_results->clear();

    int imax = bars_length.count();
    if(imax > 0)
    {
        topitem = new QTreeWidgetItem();
        topitem->setText(0, tr("Length uniformity"));
        topitem->setText(1, QString("%1%").arg(uniformity*100, 4, 'f', 2, '0'));
        ui->treeWidget_results->addTopLevelItem(topitem);

        topitem = new QTreeWidgetItem();
        topitem->setText(0, tr("Length classes"));
        ui->treeWidget_results->addTopLevelItem(topitem);
        for (int i = 0; i < imax; i++)
        {
            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setText(0, bars_length[i].name);
            item->setText(1, QString("%1%").arg(bars_length[i].value, 4, 'f', 2, '0'));
            item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
            topitem->addChild(item);
        }
    }

    imax = bars_roundness.count();
    if(imax > 0)
    {
        topitem = new QTreeWidgetItem();
        topitem->setText(0, tr("Roundness classes"));
        ui->treeWidget_results->addTopLevelItem(topitem);
        for (int i = 0; i < imax; i++)
        {
            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setText(0, bars_roundness[i].name);
            item->setText(1, QString("%1%").arg(bars_roundness[i].value, 4, 'f', 2, '0'));
            item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
            topitem->addChild(item);
        }
    }

    ui->treeWidget_results->expandAll();
}

QChart* MainWindow::makeLineChart(QList<QPointF>* lista, QList <SingleThreshold>* thresholds, QString name)
{
    QChart *chart_line = new QChart();
    chart_line->setTitle(name);
    int lista_count = lista->count();
    if(lista_count <= 0)
        return chart_line;
    int thresh_count = thresholds->count();
    if(thresh_count <= 0)
        return chart_line;
    //chart_line->setTitle(QString("Długości ziaren T = %1mm").arg(caly, 4, 'f', 2, '0' ));
    int p = 0;
    int i = 0;
    QLineSeries *lseries;
    thresh_count--;
    for(; p < thresh_count; p++)
    {
        int threshold = (*thresholds)[p].value;
        lseries = new QLineSeries(chart_line);
        for(; i < lista_count; i++)
        {
            lseries->append((*lista)[i].x(), (*lista)[i].y());
            if(i >= threshold)
                break;
        }
        lseries->setName((*thresholds)[p].name);
        chart_line->addSeries(lseries);
    }
    lseries = new QLineSeries(chart_line);
    for(; i < lista_count; i++)
    {
        lseries->append((*lista)[i].x(), (*lista)[i].y());
    }
    lseries->setName((*thresholds)[p].name);
    chart_line->addSeries(lseries);

    chart_line->createDefaultAxes();
    chart_line->axes(Qt::Horizontal).first()->setRange((int)(*lista)[0].x(), rnd((*lista)[lista_count-1].x()));
    chart_line->axes(Qt::Vertical).first()->setRange(0, rnd((*lista)[lista_count-1].y()));
    return chart_line;
}

QChart* MainWindow::makeBarChart(QList <SingleBar>* lista, QString name, unsigned int color)
{
    QChart *chart_bar = new QChart();
    QBarSeries *series = new QBarSeries(chart_bar);
    QBarSet *set = new QBarSet(name);
    double maxvalue = 0;
    QBarCategoryAxis *axisX2 = new QBarCategoryAxis();
    set->setLabelColor(Qt::black);
    set->setBorderColor(Qt::black);
    QPen set_pen(QRgb(0x000000));
    set_pen.setWidth(1);
    set->setPen(set_pen);

    set->setColor(QRgb(color));
    int imax = lista->count();
    for (int i = 0; i < imax; i++)
    {
        double v = rnd((*lista)[i].value);
        *set << v;
        if(v > maxvalue)
            maxvalue = v;
        axisX2->append((*lista)[i].name);
    }
    maxvalue = ceil(maxvalue*1.01);
    series->append(set);
    series->setLabelsVisible(true);
    series->setLabelsFormat("@value %");
    series->setLabelsPosition(QAbstractBarSeries::LabelsInsideBase);
    chart_bar->addSeries(series);
    chart_bar->createDefaultAxes();
    chart_bar->axes(Qt::Vertical).first()->setRange(0, maxvalue);
    QValueAxis *axisY = qobject_cast<QValueAxis*>(chart_bar->axes(Qt::Vertical).first());
    Q_ASSERT(axisY);
    axisY->setLabelFormat("%.1f  ");
    QAbstractAxis *axisX = qobject_cast<QAbstractAxis*>(chart_bar->axes(Qt::Horizontal).first());
    chart_bar->addAxis(axisX2, Qt::AlignBottom);
    chart_bar->removeAxis(axisX);
    return chart_bar;
}

void MainWindow::on_tableWidget_currentCellChanged(int currentRow, int, int, int)
{
    if(currentRow < 0 || ui->tableWidget->rowCount() <= 0)
        return;
    int kernel_index = ui->tableWidget->item(currentRow, 0)->data(Qt::UserRole).toInt();
    int kernel_image = ui->tableWidget->item(currentRow, 0)->data(Qt::UserRole+1).toInt();
    if(kernel_image >= 0 && kernel_index >= 0 && kernel_image < ui->tabWidget_images->count())
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(kernel_image));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                ui->tabWidget_images->setCurrentIndex(kernel_image);
                renderer->repaintContour(kernel_index);
            }
        }
    }
}

void MainWindow::mousePointed(double x, double y)
{
    int tab_index = ui->tabWidget_images->currentIndex();
    if(tab_index >= 0)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab_index));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                int index = renderer->getContourIndex(x, y);
                if(index >= 0)
                {
                    int rc = ui->tableWidget->rowCount();
                    for(int r = 0; r < rc; r++)
                    {
                        int kernel_index = ui->tableWidget->item(r, 0)->data(Qt::UserRole).toInt();
                        if(kernel_index == index)
                        {
                            int kernel_image = ui->tableWidget->item(r, 0)->data(Qt::UserRole+1).toInt();
                            if(kernel_image == tab_index)
                            {
                                blockSignals(true);
                                ui->tableWidget->setCurrentCell(r, 0);
                                renderer->repaintContour(index);
                                blockSignals(false);
                                return;
                            }
                        }
                    }
                }
                renderer->repaintContour(-1);
            }
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();
        int ulsize = urlList.size();
        if(ulsize == 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            std::string filenamelocal = fileNameRemapRead(&filename);
            if(ryzLoadParameters(filenamelocal.c_str()) == 0)
            {
                event->acceptProposedAction();
                ui->statusBar->showMessage(tr("Options loaded"), 10000);
                return;
            }
            if(loadImage(&filename))
            {
                event->acceptProposedAction();
                ui->statusBar->showMessage(tr("Image loaded"), 10000);
            }
        }
    }
}

void MainWindow::tabCloseRequested(int index)
{
    ui->tabWidget_images->removeTab(index);
    emptyReport();
}

void MainWindow::appendImage(cv::Mat& image, const QString tabname, const bool imported)
{
    QScrollArea* sa = new QScrollArea(this);
    RyzRender* rr = new RyzRender(sa);
    rr->imported = imported;

    sa->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    sa->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    sa->setMouseTracking(true);
    sa->setTabletTracking(true);
    sa->setWidgetResizable(false);
    sa->setAlignment(Qt::AlignCenter);
    sa->setWidget(rr);

    ui->tabWidget_images->addTab(sa, tabname);
    connect(rr, SIGNAL(mousePointed(double,double)), this, SLOT(mousePointed(double,double)));
    connect(rr, SIGNAL(zoomChanged(double)), this, SLOT(zoomChanged(double)));

    rr->setImage(image);
    ui->tabWidget_images->setCurrentIndex(ui->tabWidget_images->count() - 1);
    if(imported)
        ui->tabWidget_images->setTabIcon(ui->tabWidget_images->count() - 1, icon_attention);
    else
        ui->tabWidget_images->setTabIcon(ui->tabWidget_images->count() - 1, icon_accepted);
}

void MainWindow::on_actionScan_triggered()
{
    QCursor oc = cursor();
    emptyReport();
    setCursor(Qt::WaitCursor);
    try
    {
        cv::Mat src = scannerGet();
        if(! src.empty())
        {
#ifdef RGB_TO_BGR_SCAN
            cv::cvtColor(src, src, cv::COLOR_RGB2BGR);
#endif
            appendImage(src, scannerMessageStream.str().c_str(), false);
            ui->statusBar->showMessage(scannerMessageStream.str().c_str(), 10000);
        }
        else
        {
            ui->statusBar->showMessage(scannerMessageStream.str().c_str(), 10000);
        }
    }
    catch(const std::exception& e)
    {
        ui->statusBar->showMessage(e.what(), 10000);
    }
    catch(...)
    {
        ui->statusBar->showMessage(tr("Unknown exception"), 10000);
    }
    setCursor(oc);
}


void MainWindow::saveImages(const QString* fileName)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    std::vector <cv::Mat> imgl;
    std::vector <std::string> infl;
    std::vector <bool> impl;

    int tab_count = ui->tabWidget_images->count();
    for(int tab = 0; tab < tab_count; tab++)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                imgl.push_back(renderer->getImage());
                infl.push_back(ui->tabWidget_images->tabText(tab).toStdString());
                impl.push_back(renderer->imported);
            }
        }
    }
    if(PagedTiffWriter(fileNameRemapWrite(fileName).c_str(), imgl, &infl, &impl) != 0)
        ui->statusBar->showMessage(tr("Error saving images"), 10000);

    setCursor(oc);
}

void MainWindow::on_actionSave_images_triggered()
{
    QString filename = QFileDialog::getSaveFileName
                                                   (this,
                                                    tr("Save images"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr(
                                                        "Multi-page tagged image file format (*.tiff) (*.tiff)"
                                                        ));
    if( !filename.isEmpty() )
    {
        saveImages(&filename);
    }
}

void MainWindow::loadImages(const QString* fileName)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    std::vector <std::string> infl;
    std::vector <bool> impl;

    std::vector<cv::Mat> imgl = PagedTiffReader(fileNameRemapRead(fileName).c_str(), &infl, &impl);

    if(imgl.size() > 0)
    {
        unsigned int maxpage = imgl.size();
        unsigned int maxpage2 = infl.size();
        unsigned int maxpage3 = impl.size();
        if(maxpage > maxpage2)
            maxpage = maxpage2;
        if(maxpage > maxpage3)
            maxpage = maxpage3;
        for(unsigned int p = 0; p < maxpage; p++)
            appendImage(imgl[p], infl[p].c_str(), impl[p]);
    }
    else
        ui->statusBar->showMessage(tr("Error loading images"), 10000);
    setCursor(oc);
}

void MainWindow::on_actionLoad_images_triggered()
{
    QString filename = QFileDialog::getOpenFileName
                                                   (this,
                                                    tr("Load images"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr(
                                                        "Multi-page tagged image file format (*.tiff) (*.tiff)"
                                                        ));
    if( !filename.isEmpty() )
    {
        loadImages(&filename);
    }
}

bool MainWindow::loadImage(const QString* fileName)
{
    bool toret = false;
    QCursor oc = cursor();
    emptyReport();
    setCursor(Qt::WaitCursor);
    try
    {
        std::string filenamelocal = fileNameRemapRead(fileName);
        //cv::Mat& src = cvLoadImage(filenamelocal.c_str(), CV_LOAD_IMAGE_COLOR);
        cv::Mat src = cv::imread(filenamelocal.c_str(), cv::IMREAD_COLOR);
        if(! src.empty())
        {
            QString tab_name = QFileInfo(*fileName).fileName();
#ifdef RGB_TO_BGR_IMPORT
            cv::cvtColor(src, src, cv::COLOR_RGB2BGR);
#endif
            appendImage(src, tab_name, true);
            ui->statusBar->showMessage(tr("Import succeeded"), 10000);
            toret = true;
        }
        else
        {
            ui->statusBar->showMessage(tr("Cannot import"), 10000);
        }
    }
    catch(const std::exception& e)
    {
        ui->statusBar->showMessage(e.what(), 10000);
    }
    catch(...)
    {
        ui->statusBar->showMessage(tr("Unknown exception"), 10000);
    }
    setCursor(oc);
    return toret;
}

void MainWindow::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName
                                                   (this,
                                                    tr("Import image"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr(
                                                        "Image files (*.tif *.tiff *.bmp *.png *.jpg *.jpeg"
                                                        ");;All files (*)"));
    if( !filename.isEmpty() )
    {
        loadImage(&filename);
    }
}

void MainWindow::on_actionSegment_triggered()
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    emptyReport();
    int no_elements = 0;
    int count_images = 0;
    int tab_count = ui->tabWidget_images->count();
    for(int tab = 0; tab < tab_count; tab++)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                cv::Mat src = renderer->getImage();
                if(src.empty())
                    continue;

                std::vector<std::vector<cv::Point>> contours = getContours(src);
                if(contours.size() <= 0)
                    no_elements |= 2;

                renderer->setContours(contours);
                count_images++;
            }
        }
    }
    if(count_images <= 0)
        ui->statusBar->showMessage(tr("Warning: Open image first"), 10000);
    if(no_elements & 2)
        ui->statusBar->showMessage(tr("Warning: Cannot find objects"), 10000);
    if(no_elements & 4)
        ui->statusBar->showMessage(tr("Warning: Cannot separate clusters"), 10000);
    setCursor(oc);

    rate();
}

double MainWindow::rnd(double in)
{
    return round(in*100.0)/100.0;

}

void MainWindow::rate()
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    emptyReport();
    int ccount = 0;
    int tab_count = ui->tabWidget_images->count();
    for(int tab = 0; tab < tab_count; tab++)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                ccount += renderer->getContoursCount();
            }
        }
    }

    ui->tableWidget->setRowCount(ccount);
    ui->tableWidget->setSortingEnabled(false);

    int rc = 0;
    for(int tab = 0; tab < tab_count; tab++)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                int rc2 = 0;
                std::vector<std::vector<cv::Point>> contours = renderer->getContours();
                int cmax = contours.size();
                for(int c = 0; c < cmax; c++)
                {
                    QPixmap pm = renderer->getPixmap(contours[c], decimate_for_icon);
                    double length;
                    double width;
                    double area = cv::contourArea(contours[c]);
                    double concavity = cv::arcLength(contours[c], true);

                    std::vector<cv::Point> hull;
                    cv::convexHull(contours[c], hull, true, true);


                    double perimc = cv::arcLength(hull, true);
                    if(perimc > 0)
                    {
                        concavity /= perimc;
                        concavity -= 1.0;
                    }
                    else concavity = -1.0;

                    cv::RotatedRect boxx = cv::minAreaRect(contours[c]);
                    if (boxx.size.width <= boxx.size.height)
                    {
                        width = boxx.size.width;
                        length = boxx.size.height;
                    }
                    else
                    {
                        width = boxx.size.height;
                        length = boxx.size.width;
                    }
                    double proportion = 0.0;
                    if(width > 0) proportion = length/width;
                    double mmpd = 25.4/image_dpi;
                    if(rc < ccount)
                    {
                        QTableWidgetItem* item0 = new QTableWidgetItem(QIcon(pm), "");
                        item0->setData(Qt::UserRole, rc2);
                        item0->setData(Qt::UserRole+1, tab);
                        QTableWidgetItem* item1 = new QTableWidgetItem();
                        item1->setData(Qt::DisplayRole, rnd(length*mmpd));
                        QTableWidgetItem* item2 = new QTableWidgetItem();
                        item2->setData(Qt::DisplayRole, rnd(width*mmpd));
                        QTableWidgetItem* item3 = new QTableWidgetItem();
                        item3->setData(Qt::DisplayRole, rnd(area*mmpd*mmpd));

                        QTableWidgetItem* item4 = new QTableWidgetItem();
                        item4->setData(Qt::DisplayRole, rnd(proportion));
                        QTableWidgetItem* item5 = new QTableWidgetItem();
                        item5->setData(Qt::DisplayRole, rnd(concavity));

                        ui->tableWidget->setItem(rc, 0, item0);
                        ui->tableWidget->setItem(rc, 1, item1);
                        ui->tableWidget->setItem(rc, 2, item2);
                        ui->tableWidget->setItem(rc, 3, item3);
                        ui->tableWidget->setItem(rc, 4, item4);
                        ui->tableWidget->setItem(rc, 5, item5);
                    }
                    rc++;
                    rc2++;
                }
            }
        }
    }

    ui->tableWidget->setSortingEnabled(true);
    makeCharts();
    setCursor(oc);
}

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>RyżMierz</h2> " << std::endl;
    ss << "Rice analysis demo software (expires in 3 months from build date)"<< "<br>" << std::endl;
    ss << "Copyright 2019 by Piotr M. Szczypiński" << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br>" << std::endl;

    ss << "<br>This software is built with:<br>" << std::endl;
    ss << "- LibTIFF " <<  TIFFLIB_VERSION << " (BSD License) <a href=\"http://www.libtiff.org/\">http://www.libtiff.org</a> <br>" << std::endl;
    ss << "- OpenCV " <<   CV_VERSION << " (3-clause BSD License) <a href=\"http://opencv.org/\">http://opencv.org</a> <br>" << std::endl;
    ss << "- SSH2 Library (BSD License) <a href=\"https://www.libssh2.org/\">https://www.libssh2.org/</a> <br>" << std::endl;
    ss << "- Oxygen Icons by The Oxygen Team (LGPL) <a href=\"https://github.com/pasnox/oxygen-icons-png\">https://github.com/pasnox/oxygen-icons-png</a> <br>" << std::endl;
    ss << "<br>This software requires:<br>" << std::endl;
    ss << "- Qt " << QT_VERSION_STR << " (LGPL) <a href=\"https://www.qt.io/developers/\">https://www.qt.io/developers</a> <br>" << std::endl;

    QMessageBox::about(this, "About Ryż", ss.str().c_str());
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionSave_triggered()
{
    QString filename = QFileDialog::getSaveFileName
                                                   (this,
                                                    tr("Save report"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr(
                                                        "Portable Document (*.pdf) (*.pdf);;"
                                                        "Open Document (*.odt) (*.odt);;"
                                                        "All files (*)"));
    if( !filename.isEmpty() )
    {
        saveReport(&filename);
    }
}

void MainWindow::saveReport(const QString* fileName)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

//    QFileInfo fi(*fileName);
//    if(fi.suffix().compare("pdf", Qt::CaseInsensitive) == 0)
//    {
        //QTextDocument* r = createReport();
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(*fileName);
        printer.setPageSize(QPageSize(QPageSize::A4));

        createReport(&printer, ui->checkBox_save_charts->isChecked(), ui->checkBox_save_list->isChecked());

        //r->print(&printer);
        //delete r;
//    }
//    else
//    {
//        QTextDocumentWriter writer(*fileName);
//        QTextDocument* r = createReport();
//        writer.write(r);
//        delete r;
//    }
    setCursor(oc);
}


void MainWindow::on_actionPrint_triggered()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setPageSize(QPageSize(QPageSize::A4));
    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("Print report"));
    if (dialog->exec() != QDialog::Accepted)
        return;
    createReport(&printer, ui->checkBox_print_charts->isChecked(), ui->checkBox_print_list->isChecked());
//    QTextDocument* r = createReport();
//    printer.setPageSize(QPageSize(QPageSize::A4));
//    r->print(&printer);
//    delete r;
}


void MainWindow::paintTreeData(QPainter* painter, QRect* linerect, QTreeWidget* treeWidget)
{
    int topCount = treeWidget->topLevelItemCount();
    for (int i = 0; i < topCount; i++)
    {
        QTreeWidgetItem *item = treeWidget->topLevelItem(i);

        linerect->translate(0, linerect->height());
        painter->drawText(*linerect, Qt::AlignVCenter | Qt::AlignLeft, QString("%1: %2").arg(item->text(0)).arg(item->text(1)));

        QRect indented = *linerect;
        indented.setX(indented.x()+indented.height()*2);

        int childCount = item->childCount();
        for (int ii = 0; ii < childCount; ii++)
        {
            QTreeWidgetItem *citem = item->child(ii);
            linerect->translate(0, indented.height());
            indented.translate(0, indented.height());
            painter->drawText(indented, Qt::AlignVCenter | Qt::AlignLeft, QString("%1: %2").arg(citem->text(0)).arg(citem->text(1)));
        }
        if(childCount > 0 && i+1 < topCount)
        {
            linerect->translate(0, indented.height()/2);
            indented.translate(0, indented.height()/2);
        }
    }
}

void MainWindow::createReport(QPrinter* printer, bool printCharts, bool printTable)
{
    QPainter painter(printer);
    //QMargins marg = printer->pageLayout().marginsPixels(printer->resolution());
    QRect pageviewport = painter.viewport();
    int margin = 600; //600 ->1/2 cala (pageviewport.right()-pageviewport.left())/10;
    QMargins margins(margin, margin, margin, margin);
    QRect pagerect = pageviewport.marginsRemoved(margins);
    QPen pen(painter.pen());
    pen.setWidth(12);
    painter.setPen(pen);

//    printf("Viewport %i, %i, %i, %i\n",
//           pageviewport.left(),
//           pageviewport.top(),
//           pageviewport.right(),
//           pageviewport.bottom()
//          );
//    fflush(stdout);

// *17 - single line
    int fontBodySize = 12;
    int fontBodyPx = fontBodySize*18;
    int fontTableSize = 10;
    int fontTablePx = fontTableSize*18;
    int fontHeaderSize = 16;
    int fontHeaderPx = fontHeaderSize*18;
    QFont fontBody("Arial", fontBodySize, -1, false);
    QFont fontBodyItal("Arial", fontBodySize, -1, true);
    QFont fontTable("Arial", fontTableSize, -1, false);
    QFont fontHeader("Arial", fontHeaderSize, -1, false);

    painter.begin(printer);

    QRect linerect;
    QRect indented;
    linerect = pagerect;
    linerect.setHeight(fontHeaderPx);
    painter.setFont(fontHeader);
    painter.drawText(linerect, Qt::AlignCenter, tr("Report"));
    linerect.translate(0, linerect.height());
    painter.setFont(fontBody);
    linerect.setHeight(fontBodyPx);

    paintTreeData(&painter, &linerect, ui->treeWidget);
    linerect.translate(0, linerect.height());
    linerect.translate(0, linerect.height());

    indented = linerect;
    indented.setX(indented.x()+fontBodyPx*2);
    if(t_count == 0)
    {
        painter.drawPixmap(linerect.x(), linerect.y(), linerect.height(), linerect.height(), icon_attention_big.pixmap(48));
        painter.drawText(indented, Qt::AlignVCenter | Qt::AlignLeft, (tr("Reference length %1mm default or entered manually")).arg(t_value));
    }
    else if(t_count < 0)
    {
        painter.drawPixmap(linerect.x(), linerect.y(), linerect.height(), linerect.height(), icon_attention_big.pixmap(48));
        painter.drawText(indented, Qt::AlignVCenter | Qt::AlignLeft, (tr("Reference length %1mm averaged earlier for %2 kernels")).arg(t_value).arg(t_count));
    }
    else if(t_count < 100)
    {
        painter.drawPixmap(linerect.x(), linerect.y(), linerect.height(), linerect.height(), icon_attention_big.pixmap(48));
        painter.drawText(indented, Qt::AlignVCenter | Qt::AlignLeft, (tr("Reference length %1mm averaged for %2 kernels")).arg(t_value).arg(t_count));
    }
    else
    {
        painter.drawText(linerect, Qt::AlignVCenter | Qt::AlignLeft, (tr("Reference length %1mm averaged for %2 kernels")).arg(t_value).arg(t_count));
    }
    linerect.translate(0, linerect.height());
    indented = linerect;
    indented.setX(indented.x()+fontBodyPx*2);
    int tab_count = ui->tabWidget_images->count();
    for(int tab = 0; tab < tab_count; tab++)
    {
        bool imported = true;
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                imported = renderer->imported;
            }
        }
        if(imported)
        {
            painter.drawPixmap(linerect.x(), linerect.y(), linerect.height(), linerect.height(), icon_attention_big.pixmap(48));
            painter.drawText(indented, Qt::AlignVCenter | Qt::AlignLeft, (tr("Imported image: %1")).arg(ui->tabWidget_images->tabText(tab)));
            linerect.translate(0, linerect.height());
            indented.translate(0, linerect.height());
        }
    }
    linerect.translate(0, linerect.height());

    paintTreeData(&painter, &linerect, ui->treeWidget_results);
    linerect.translate(0, linerect.height());
    linerect.translate(0, linerect.height());

    if(ui->plainTextEdit_comments->toPlainText().length() > 0)
    {
        painter.drawText(linerect, Qt::AlignVCenter | Qt::AlignLeft, tr("Comments: "));
        linerect.translate(0, linerect.height());
        //linerect.setHeight(fontBodyPx*6);
        painter.setFont(fontBodyItal);
        indented = linerect;
        indented.setX(indented.x()+fontBodyPx*2);
    //    painter.drawText(indented.x(), indented.y(), ui->plainTextEdit_comments->toPlainText());
        indented = painter.boundingRect(indented, Qt::AlignTop | Qt::AlignLeft | Qt::TextWordWrap, ui->plainTextEdit_comments->toPlainText());
        painter.drawText(indented, Qt::AlignTop | Qt::AlignLeft | Qt::TextWordWrap, ui->plainTextEdit_comments->toPlainText());
        linerect.translate(0, indented.height());
    }
    painter.setFont(fontBody);

    if(printCharts)
    {
        printer->newPage();

        linerect = pagerect;
        linerect.setHeight(fontHeaderPx);
        painter.setFont(fontHeader);

        QList<QPointF> line;
        QList <SingleThreshold> line_thresholds;
        QList <SingleBar> bars_length;
        QList <SingleBar> bars_roundness;

        unsigned int color1;
        unsigned int color2;
        if(ui->radioButton_weight->isChecked())
        {
            painter.drawText(linerect, Qt::AlignCenter, tr("Contribution in estimated weight"));
            makeChartsDataArea(&line, &line_thresholds, &bars_length, &bars_roundness);
            color1 = 0xb4ffd2;
            color2 = 0x9fe1ba;
        }
        else
        {
            painter.drawText(linerect, Qt::AlignCenter, tr("Contribution in pieces"));
            makeChartsData(&line, &line_thresholds, &bars_length, &bars_roundness);
            color1 = 0xffebc1;
            color2 = 0xe1cfaa;
        }
        linerect.translate(0, linerect.height());
        linerect.setHeight((pagerect.height()-fontHeaderPx)/3);
        //linerect.setWidth(pagerect.width()/2);



        QChart* lchart = makeLineChart(&line, &line_thresholds, tr("Length uniformity"));
        lchart->resize(800, 400);
        QChart* bchart = makeBarChart(&bars_length, tr("Length classes"), color1);
        bchart->resize(800, 400);
        QChart* bcharts = makeBarChart(&bars_roundness, tr("Roundness classes"), color2);
        bcharts->resize(800, 400);

        QChartView* linechart = new QChartView(lchart);
        linechart->setRenderHint(QPainter::TextAntialiasing);
        linechart->show();
        linechart->render(&painter, linerect);
        delete linechart;
        linerect.translate(0, linerect.height());

        QChartView* barchart = new QChartView(bchart);
        barchart->setRenderHint(QPainter::TextAntialiasing);
        barchart->show();
        barchart->render(&painter, linerect);
        delete barchart;
        linerect.translate(0, linerect.height());

        QChartView* barcharts = new QChartView(bcharts);
        barcharts->setRenderHint(QPainter::TextAntialiasing);
        barcharts->show();
        barcharts->render(&painter, linerect);
        delete barcharts;
//        linerect.translate(0, linerect.height());
    }

    if(printTable)
    {
        bool newpage = true;
        painter.setFont(fontTable);

        QRect klp;
        int ki[3];
        QRect kl[3];

        int cc = 0;
        bool leftcol = true;
        int count = ui->tableWidget->rowCount();
        for (int c = 0; c < count; c++, cc++)
        {
            if(newpage)
            {
                newpage = false;
                cc = 0;
                double xmax = pagerect.width()/2;
                double xl = pagerect.x();

                if(leftcol)
                {
                    printer->newPage();
                    leftcol = false;
                }
                else
                {
                    xl += xmax;
                    painter.drawLine(xl, pagerect.y(), xl, pagerect.bottom());
                    leftcol = true;
                }
                double xs;
                int yt = pagerect.y();
                int yb = yt+fontTablePx;
                xs = xmax*0.1;
                klp.setCoords(xl, yt, xl+xs, yb); xl+=xs;

                xs = fontTablePx*3.1;
                ki[0] = xl+xs/2;
                ki[1] = ki[0]+xs;
                ki[2] = ki[1]+xs;

                xs = xs*3.0;
                QRect kit;
                kit.setCoords(xl, yt, xl+xs, yb); xl+=xs;
                xs = xmax*0.15;
                kl[0].setCoords(xl, yt, xl+xs, yb); xl+=xs;
                kl[1].setCoords(xl, yt, xl+xs, yb); xl+=xs;
                kl[2].setCoords(xl, yt, xl+xs, yb);

                painter.drawText(klp, Qt::AlignVCenter | Qt::AlignRight, QString("#"));
                painter.drawText(kit, Qt::AlignVCenter | Qt::AlignCenter, ELEMENT_NAGLOWKA_0);
                painter.drawText(kl[0], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_NAGLOWKA_1);
                painter.drawText(kl[1], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_NAGLOWKA_2);
                painter.drawText(kl[2], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_NAGLOWKA_3);
                painter.drawLine(klp.x()+fontTablePx, klp.bottom(), klp.x()+xmax-fontTablePx, klp.bottom());

                klp.translate(0, fontTablePx*2);
                kl[0].translate(0, fontTablePx*2);
                kl[1].translate(0, fontTablePx*2);
                kl[2].translate(0, fontTablePx*2);
            }

            painter.drawText(klp, Qt::AlignVCenter | Qt::AlignRight, QString("%1").arg(c+1));
            painter.drawText(kl[0], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_KOLUMNY_1);
            painter.drawText(kl[1], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_KOLUMNY_2);
            painter.drawText(kl[2], Qt::AlignVCenter | Qt::AlignRight, ELEMENT_KOLUMNY_3);

            int kernel_index = ui->tableWidget->item(c, 0)->data(Qt::UserRole).toInt();
            int kernel_image = ui->tableWidget->item(c, 0)->data(Qt::UserRole+1).toInt();
            if(kernel_image >= 0 && kernel_index >= 0 && kernel_image < ui->tabWidget_images->count())
            {
                QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(kernel_image));
                if(scroll != NULL)
                {
                    RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
                    if(renderer != NULL)
                    {

                        QPixmap pm = renderer->getPixmap(kernel_index, 1);
                        double zoom = 3;
                        double res = fontTablePx*3 / pm.height();
                        double resx = (ki[1]-ki[0]) / pm.width();
                        if(res > resx)
                            res = resx;
                        if(res >= zoom)
                        {
                            painter.drawPixmap(ki[cc%3]-pm.width()*zoom/2, klp.y()+fontTablePx/2-pm.height()*zoom/2, pm.width()*zoom, pm.height()*zoom, pm);
                        }
                        else
                        {
                            painter.drawPixmap(ki[cc%3]-pm.width()*res/2, klp.y()+fontTablePx/2-pm.height()*res/2, pm.width()*res, pm.height()*res, pm);
                        }
                    }
                }
            }

            klp.translate(0, fontTablePx);
            kl[0].translate(0, fontTablePx);
            kl[1].translate(0, fontTablePx);
            kl[2].translate(0, fontTablePx);

            if(klp.bottom() > pagerect.bottom())
                newpage = true;
        }
    }
    painter.end();
}

/*
QTextDocument* MainWindow::createReport(void)
{
    QTextDocument* document = new QTextDocument();
    QTextCursor cursor(document);
    cursor.insertText(QObject::tr("Report . . . . . . . .\n"));
    QTextTableFormat tableFormat;
    tableFormat.setCellPadding(5);
    tableFormat.setHeaderRowCount(1);
    tableFormat.setBorderStyle(
                QTextFrameFormat::BorderStyle_Solid);
    tableFormat.setWidth(QTextLength(
                             QTextLength::PercentageLength, 100));
    cursor.insertTable(1, 4, tableFormat);
    cursor.insertText(QObject::tr("Object"));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QObject::tr("Length [mm]"));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QObject::tr("Width [mm]"));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QObject::tr("Area [mm2]"));
    QTextTable *table = cursor.currentTable();
    int count = ui->tableWidget->rowCount();
    for (int c = 0; c < count; c++)
    {
        CvSeq* cr = getContourFromRow(c);
        CvRect box = cvBoundingRect(cr);
        QPixmap pm = makePixmap(cr, box, 1);

        table = cursor.currentTable();
        QTableWidgetItem* it1 = ui->tableWidget->item(c, 1);
        QTableWidgetItem* it2 = ui->tableWidget->item(c, 2);
        QTableWidgetItem* it3 = ui->tableWidget->item(c, 3);
        table->appendRows(1);
        //cursor.movePosition(QTextCursor::PreviousRow);
        //cursor.movePosition(QTextCursor::NextCell);
        //cursor.movePosition(QTextCursor::PreviousRow);
        //cursor.movePosition(QTextCursor::Left);
        cursor.movePosition(QTextCursor::Left);
        cursor.movePosition(QTextCursor::Left);
        cursor.movePosition(QTextCursor::Left);
        cursor.insertImage(pm.toImage());
        cursor.movePosition(QTextCursor::NextCell);
        cursor.insertText(it1->text());
        cursor.movePosition(QTextCursor::NextCell);
        cursor.insertText(it2->text());
        cursor.movePosition(QTextCursor::NextCell);
        cursor.insertText(it3->text());
    }
    return document;
}
*/

void MainWindow::on_actionRemove_item_triggered()
{
    QSet<int> srows;
    QList<QTableWidgetItem *> selected = ui->tableWidget->selectedItems();
    QTableWidgetItem * item;
    foreach(item, selected)
        srows.insert(item->row());
    QList<int> rows(srows.begin(), srows.end());// deprecated: rows = srows.toList();
    std::sort(rows.begin(), rows.end(), std::greater<int>());

    blockSignals(true);
    ui->tableWidget->blockSignals(true);
    foreach(int row, rows)
        ui->tableWidget->removeRow(row);
    ui->tableWidget->blockSignals(false);
    blockSignals(false);
    //on_tableWidget_currentCellChanged(ui->tableWidget->currentRow(), 0, 0, 0);
    //on_tableWidget_currentCellChanged(-1, 0, 0, 0);
    makeCharts();
}

void MainWindow::on_actionCompute_T_triggered()
{
    int tt_count;
    double tt_value;
    QSet<int> srows;
    QList<QTableWidgetItem *> selected = ui->tableWidget->selectedItems();
    QTableWidgetItem * item;
    foreach(item, selected)
        srows.insert(item->row());
    QList<int> rows(srows.begin(), srows.end());// deprecated: rows = srows.toList();

    tt_value = 0.0;
    foreach(int r, rows)
        tt_value += ui->tableWidget->item(r, 1)->data(Qt::DisplayRole).toDouble();

    tt_count = rows.count();
    tt_value /= tt_count;
    blockSignals(true);
    ui->doubleSpinBox->setValue(tt_value);
    if(tt_count >= 100)
        ui->label_reference_state->setPixmap(icon_accepted.pixmap(16));
    else
        ui->label_reference_state->setPixmap(icon_attention.pixmap(16));
    makeCharts();
    blockSignals(false);

    t_value = tt_value;
    t_count = tt_count;
}

void MainWindow::on_actionService_configuration_triggered()
{
    ServiceConfig dialog(this);
    dialog.setSftpConfig(serviceConfig);
    if(dialog.exec() == QDialog::Accepted)
    {
        serviceConfig = dialog.getSftpConfig();
        saveServiceOptions("ryzOptions.json");
    }
}

void MainWindow::on_actionClose_triggered()
{
    int tab_count = ui->tabWidget_images->count();
    for(int tab = 0; tab < tab_count; tab++)
    {
        ui->tabWidget_images->removeTab(tab);
    }
    emptyReport();
}

void MainWindow::on_actionUpload_to_server_triggered()
{
    QString temp_path = QDir::tempPath();
    QDateTime datetime = QDateTime::currentDateTime();
    QString filename_stub = datetime.toString(tr("yyyy-MM-dd-hh-mm-ss"));

    if(ui->checkBox_upload->isChecked())
    {
        QString filename = temp_path;
        filename += "/img-";
        filename += filename_stub;
        filename += ".tiff";
        saveImages(&filename);
        sendToSftp(&serviceConfig, &filename);
    }

    QString filename = temp_path;
    filename += "/rep-";
    filename += filename_stub;
    filename += ".pdf";
    saveReport(&filename);
    sendToSftp(&serviceConfig, &filename);
}
void MainWindow::on_pushButton_upload_clicked()
{
    on_actionUpload_to_server_triggered();
}

void MainWindow::on_pushButton_find_kernels_clicked()
{
    on_actionSegment_triggered();
}

void MainWindow::on_pushButton_save_images_clicked()
{
    on_actionSave_images_triggered();
}

void MainWindow::on_pushButton_save_report_clicked()
{
    on_actionSave_triggered();
}

void MainWindow::on_pushButton_print_clicked()
{
    on_actionPrint_triggered();
}

void MainWindow::on_pushButton_remove_item_clicked()
{
    on_actionRemove_item_triggered();
}

void MainWindow::on_pushButton_scan_clicked()
{
    on_actionScan_triggered();
}

void MainWindow::on_pushButton_close_all_clicked()
{
    on_actionClose_triggered();
}

void MainWindow::on_pushButton_compute_t_clicked()
{
    on_actionCompute_T_triggered();
}

void MainWindow::zoomChanged(double zoom)
{
    int var = 8.7 * log(zoom);
    ui->horizontalSlider_zoom->setValue(var);
}

void MainWindow::on_horizontalSlider_zoom_actionTriggered(int action)
{
    if(action)
    {
        int tab_index = ui->tabWidget_images->currentIndex();
        if(tab_index >= 0)
        {
            QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab_index));
            if(scroll != NULL)
            {
                RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
                if(renderer != NULL)
                {
                    double zoom_new = exp((double)ui->horizontalSlider_zoom->value()/8.7);
                    renderer->setZoom(zoom_new);
                }
            }
        }
    }
}

void MainWindow::on_pushButton_zoomout_clicked()
{
    //ui->horizontalSlider_zoom->triggerAction(QAbstractSlider::SliderSingleStepSub);
    ui->horizontalSlider_zoom->setValue(ui->horizontalSlider_zoom->value()-1);
    on_horizontalSlider_zoom_actionTriggered(QAbstractSlider::SliderSingleStepSub);
}

void MainWindow::on_pushButton_zoomin_clicked()
{
    //ui->horizontalSlider_zoom->triggerAction(QAbstractSlider::SliderSingleStepAdd);
    ui->horizontalSlider_zoom->setValue(ui->horizontalSlider_zoom->value()+1);
    on_horizontalSlider_zoom_actionTriggered(QAbstractSlider::SliderSingleStepAdd);
}

void MainWindow::on_pushButton_zoomorig_clicked()
{
    ui->horizontalSlider_zoom->setValue(0);
    on_horizontalSlider_zoom_actionTriggered(QAbstractSlider::SliderMove);
}

void MainWindow::on_tabWidget_images_currentChanged(int tab_index)
{
    if(tab_index >= 0)
    {
        QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab_index));
        if(scroll != NULL)
        {
            RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
            if(renderer != NULL)
            {
                int var = 8.7 * log(renderer->getZoom());
                ui->horizontalSlider_zoom->setValue(var);
            }
        }
    }
}

void MainWindow::on_tableWidget_itemSelectionChanged()
{
    QList<QTableWidgetSelectionRange> ranges = ui->tableWidget->selectedRanges();
    int allc = 0;
    int rc = ranges.count();
    for(int r = 0; r < rc; r++)
    {
        allc += ranges[r].rowCount();
    }
    ui->statusBar->showMessage(QString(tr("Number of selected items: %1")).arg(allc), 10000);
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    t_count = 0;
    t_value = arg1;
    ui->label_reference_state->setPixmap(icon_attention.pixmap(16));
}
void MainWindow::on_doubleSpinBox_editingFinished()
{
    makeCharts();
}

void MainWindow::on_radioButton_weight_toggled(bool)
{
    makeCharts();
}

#include "roiio.h"
void MainWindow::on_actionSave_masks_triggered()
{
    const unsigned int RoiDefaultColors[16] =
     {
         0x00FF0000, 0x0000FF00, 0x000000FF,
         0x0000FFFF, 0x00FF00FF, 0x00FFFF00,
         0x00FF8000, 0x00FF0080, 0x0080FF00,
         0x0000FF80, 0x008000FF, 0x000080FF,
         0x00FFC400, 0x00C4FF00, 0x00C400FF,
         0x0000FFC4
     };


     int tab_index = ui->tabWidget_images->currentIndex();
     if(tab_index >= 0)
     {
         QScrollArea* scroll = qobject_cast<QScrollArea*>(ui->tabWidget_images->widget(tab_index));
         if(scroll != NULL)
         {
             RyzRender* renderer = qobject_cast<RyzRender*>(scroll->widget());
             if(renderer != NULL)
             {

                 QString filename = QFileDialog::getSaveFileName
                                                                (this,
                                                                 tr("Save masks"),
                                                                 NULL,//domyslna nazwa pliku
                                                                 tr(
                                                                     "Multi-page tagged image file format (*.tiff) (*.tiff)"
                                                                     ));
                 if( !filename.isEmpty() )
                 {
                     std::vector<TMazdaRoi*> outroi;
                     std::vector<std::vector<cv::Point>> contours = renderer->getContours();
                     int cmax = contours.size();
                     for(int c = 0; c < cmax; c++)
                     {
                         cv::Rect box = cv::boundingRect(contours[c]);
                         cv::Mat maska(box.height, box.width, CV_8U);
                         maska.setTo(cv::Scalar(0));
                         cv::drawContours(maska, contours, c, cv::Scalar(255), cv::FILLED, cv::LINE_AA, cv::Mat(), 1, cv::Point(-box.x, -box.y));


                         int begin[TMazdaRoi::Dimensions];
                         int end[TMazdaRoi::Dimensions];
                         begin[0] = 0;
                         begin[1] = 0;
                         end[0] = box.width-1;
                         end[1] = box.height-1;
                         TMazdaRoi* roi = new TMazdaRoi(begin, end);
                         MazdaRoiIterator<TMazdaRoi> iterator(roi);
                         for(int y = 0; y < box.height; y++)
                         {
                             unsigned char* pm = ((unsigned char *)maska.data + (size_t)y * maska.step);
                             unsigned char* max = pm + (size_t)box.width;
                             for(; pm < max; pm++)
                             {
                                 if(*pm > 0)
                                 {
                                     iterator.SetPixel();
                                 }
                                 ++iterator;
                             }
                         }
                         char name[16];
                         sprintf(name, "ryz%.4i", (int)outroi.size()+1);
                         begin[0] = box.x;
                         begin[1] = box.y;
                         roi->SetBegin(begin);
                         roi->SetName(std::string(name));
                         roi->SetColor(RoiDefaultColors[(unsigned int)outroi.size() % 16]);
                         outroi.push_back(roi);
                     }
                     ROIPagedTiffWriter(fileNameRemapWrite(&filename).c_str(), &outroi);
                 }
             }
         }
     }
}
