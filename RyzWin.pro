#-------------------------------------------------
#
# Project created by QtCreator 2019-03-12T17:14:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += charts
QT += printsupport

# sudo apt install libqt5charts5-dev
# sudo apt install qml-module-qtcharts

TARGET = RyzMierz
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DEFINES += RGB_TO_BGR_SCAN
DEFINES += RGB_TO_BGR_IMPORT
DEFINES += HAVE_WINSOCK2_H
//#define HAVE_SYS_SOCKET_H
//#define HAVE_UNISTD_H
//#define HAVE_NETINET_IN_H
//#define HAVE_ARPA_INET_H


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        ryz.cpp \
    ryzrender.cpp \
    scannerwia.cpp \
    serviceconfig.cpp \
    filenameremap.cpp \
    paramtreewidget.cpp \
    tiffpagedrgb.cpp

HEADERS += \
        mainwindow.h \
        ryz.h \
    ryzrender.h \
    scanner.h \
    serviceconfig.h \
    filenameremap.h \
    paramtreewidget.h \
    tiffpagedrgb.h

FORMS += \
        mainwindow.ui \
    serviceconfig.ui

RESOURCES += \
    ryzres.qrc

TRANSLATIONS = ryzmierz_pl.ts


RESOURCES += \
    ryzres.qrc
    DEFINES += _USE_MATH_DEFINES

    DEFINES += _ITERATOR_DEBUG_LEVEL=0
    QMAKE_CXXFLAGS_DEBUG -= /MDd
    QMAKE_CXXFLAGS_DEBUG += /MT
    QMAKE_CFLAGS_DEBUG -= /MDd
    QMAKE_CFLAGS_DEBUG += /MT

    QMAKE_CFLAGS -= /MDd
    QMAKE_CXXFLAGS -= /MDd
    QMAKE_CXXFLAGS_RELEASE -= /MDd
    QMAKE_CFLAGS -= /MTd
    QMAKE_CXXFLAGS -= /MTd
    QMAKE_CXXFLAGS_RELEASE -= /MTd
    QMAKE_CFLAGS -= /MD
    QMAKE_CXXFLAGS -= /MD
    QMAKE_CXXFLAGS_RELEASE -= /MD
    QMAKE_CFLAGS += /MT /O2
    QMAKE_CXXFLAGS += /MT /O2
    QMAKE_CXXFLAGS_RELEASE += /MT /O2

    INCLUDEPATH += C:/Programowanie/usr/include
    LIBS += C:/Programowanie/usr/lib/alglib.lib

    INCLUDEPATH += C:\Programowanie\Sources\libssh2-1.9.0\include
    LIBS += C:\Programowanie\Sources\libssh2-1.9.0\build\src\Release\libssh2.lib

    INCLUDEPATH += C:/Programowanie/usr/include
    LIBS += User32.lib
    LIBS += vfw32.lib
    LIBS += comctl32.lib
    LIBS += setupapi.lib
    LIBS += ws2_32.lib
    LIBS += wiaguid.lib
    LIBS += ole32.lib
    LIBS += OleAut32.lib
    LIBS += shlwapi.lib

    LIBS += C:/Programowanie/usr/opencv3/lib/ippicvmt.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_stitching310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_superres310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_ts310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_video310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_videoio310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_videostab310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/zlib.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/IlmImf.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/libjasper.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/libjpeg.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/libpng.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/libtiff.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/libwebp.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_calib3d310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_core310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_features2d310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_flann310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_highgui310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_imgcodecs310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_imgproc310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_ml310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_objdetect310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_photo310.lib
    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_shape310.lib


#INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/

#    LIBS += \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_core.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_imgproc.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_videostab.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_stitching.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_calib3d.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_features2d.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_superres.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_objdetect.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_shape.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_video.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_photo.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_ml.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_flann.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_highgui.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_videoio.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_imgcodecs.a

#    LIBS += \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libzlib.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libippiw.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libippicv.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libittnotify.a
