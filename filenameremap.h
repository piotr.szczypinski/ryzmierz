#ifndef FILENAMEREMAP_H
#define FILENAMEREMAP_H


#include <QString>
#include <string>

std::string fileNameRemapWrite(const QString *input);
std::string fileNameRemapRead(const QString *input);

#endif // FILENAMEREMAP_H
