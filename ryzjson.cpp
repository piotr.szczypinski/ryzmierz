/*
 * main.cpp - program do analizy ziaren ryżu
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <time.h>
#include "opencv2/imgproc.hpp"

//Parameters
int debug_mode = 0;

int grey_level_threshold = 70;
//const int thresholding_algorithm = cv::THRESH_BINARY | cv::THRESH_OTSU;
int thresholding_algorithm = cv::THRESH_BINARY;
int structuring_element_radius = 2;
double concavity_threshold = -0.038;
double pairing_grain_thickness = 75;
double maximum_grain_area = 14000;
double minimum_grain_area = 500;



unsigned int decimate_for_icon = 3;
double image_dpi = 600;
int max_cpoints_per_contour = 0;


#include <QJsonObject>
void ryzJsonWrite(QJsonObject &json)
{
    json["grey_level_threshold"] = grey_level_threshold;
    json["thresholding_algorithm"] = thresholding_algorithm;

    json["debug_mode"] = debug_mode;
    json["decimate_for_icon"] = (int)decimate_for_icon;
    json["image_dpi"] = image_dpi;
}

void ryzJsonRead(const QJsonObject &json)
{
    if(json.contains("grey_level_threshold"))
        grey_level_threshold = json["grey_level_threshold"].toInt();
    if(json.contains("thresholding_algorithm"))
        thresholding_algorithm = json["thresholding_algorithm"].toInt();

    if(json.contains("debug_mode"))
        debug_mode = json["debug_mode"].toDouble();
    if(json.contains("decimate_for_icon"))
        decimate_for_icon = json["decimate_for_icon"].toInt();
    if(json.contains("image_dpi"))
        image_dpi = json["image_dpi"].toDouble();
}


int ryzLoadParameters(std::istream* file)
{
    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    std::string sline;
    unsigned int l = 0;
    while(std::getline(*file, sline))
    {
        l++;
        std::stringstream ss(sline);
        std::string name;
        double value;
        name.clear();
        ss >> std::skipws >> name;
        ss >> std::skipws >> value;
        if(name.length() <= 0)
            continue;
        if(name == "grey_level_threshold")
            grey_level_threshold = value;

        else if(name == "debug_mode")
            debug_mode = value;
        else if(name == "decimate_for_icon")
            decimate_for_icon = value;
        else if(name == "image_dpi")
            image_dpi = value;
        else
        {
            setlocale(LC_ALL, prevloc);
            free(prevloc);
            return l;
        }
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    return 0;
}

int ryzLoadParameters(const char* fileName)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open()) return -1;
    if (!file.good()) return -1;
    return ryzLoadParameters(&file);
}

