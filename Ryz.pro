#-------------------------------------------------
#
# Project created by QtCreator 2019-03-12T17:14:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += charts
QT += printsupport

# sudo apt install libqt5charts5-dev
# sudo apt install qml-module-qtcharts

TARGET = RyzMierz
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# DEFINES += RGB_TO_BGR_SCAN
DEFINES += RGB_TO_BGR_IMPORT

DEFINES += HAVE_SYS_SOCKET_H
DEFINES += HAVE_UNISTD_H
DEFINES += HAVE_NETINET_IN_H
DEFINES += HAVE_ARPA_INET_H

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        ryz.cpp \
    ryzrender.cpp \
    scanner.cpp \
    serviceconfig.cpp \
    filenameremap.cpp \
    paramtreewidget.cpp \
    tiffpagedrgb.cpp

HEADERS += \
        mainwindow.h \
        ryz.h \
    ryzrender.h \
    scanner.h \
    serviceconfig.h \
    filenameremap.h \
    paramtreewidget.h \
    tiffpagedrgb.h

FORMS += \
        mainwindow.ui \
    serviceconfig.ui

RESOURCES += \
    ryzres.qrc

TRANSLATIONS = ryzmierz_pl.ts

#INCLUDEPATH += /home/piotr/Program/qmazda/MzShared
INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/alglib
LIBS += /home/piotr/Program/ThirdParty/usr/lib/libalglib.a
#LIBS += -ltiff
LIBS += -lopencv_core
LIBS += -lopencv_imgproc
LIBS += -lopencv_highgui
LIBS += -lopencv_imgcodecs
LIBS += -lssh2
LIBS += -lsane
#include(../qmazda/Pri/opencv.pri)

#INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/

#    LIBS += \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_core.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_imgproc.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_videostab.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_stitching.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_calib3d.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_features2d.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_superres.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_objdetect.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_shape.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_video.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_photo.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_ml.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_flann.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_highgui.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_videoio.a \
#        /home/piotr/Program/ThirdParty/usr/lib/libopencv_imgcodecs.a

#    LIBS += \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libzlib.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libippiw.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libippicv.a \
#        /home/piotr/Program/ThirdParty/usr/share/OpenCV/3rdparty/lib/libittnotify.a

