#ifndef ROIIO_H
#define ROIIO_H


#include "../qmazda/SharedImage/mazdaroi.h"

#include "tiffio.h"
typedef MazdaRoi<unsigned int, 2> TMazdaRoi;

static void SwapBitsInBytesInBuffer(unsigned char* src, unsigned char* dst, int bytesnumber)
{
    unsigned char* srctemp = src;
    unsigned char* dsttemp = dst;
    for(int i = 0; i < bytesnumber; i++)
    {
        unsigned char b = *srctemp;
        b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
        b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
        b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
        *dsttemp = b;
        srctemp++;
        dsttemp++;
    }
}

static void ROITiffWriter(TIFF* tiff, TMazdaRoi* source, unsigned int page, unsigned int pages)
{
    unsigned int y = 0;
    unsigned int zero = 0;
    TMazdaRoi* dest;
    if(source->IsEmpty())
    {
        dest = new TMazdaRoi();
        dest->SetColor(source->GetColor());
        dest->SetName(source->GetName());
    }
    else dest = MazdaRoiResizer< TMazdaRoi >::Upsize(source);

    unsigned int size[TMazdaRoi::Dimensions];
    for(zero = 0; zero < TMazdaRoi::Dimensions; zero++) size[zero] = 0;

    if(! dest->IsEmpty())
    {
        dest->GetSize(size);
        TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, size[0]);
        TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, size[1]);
        if(TMazdaRoi::Dimensions > 2)
            if(size[2] > 1)
                TIFFSetField(tiff, TIFFTAG_IMAGEDEPTH, size[2]);
    }
    else
    {
        TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, 1);
        TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, 1);
    }
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
    TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)100);
    TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)100);
    TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);

    uint16_t rlut[2];
    uint16_t glut[2];
    uint16_t blut[2];
    blut[0] = 0;
    glut[0] = 0;
    rlut[0] = 0;

    unsigned int rgbacolor = dest->GetColor();
    blut[1] = (rgbacolor&0xff) * 0x0101;
    glut[1] = ((rgbacolor>>8)&0xff) * 0x0101;
    rlut[1] = ((rgbacolor>>16)&0xff) * 0x0101;

    TIFFSetField(tiff, TIFFTAG_COLORMAP, rlut, glut, blut);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_PALETTE);
    //COMPRESSION_CCITTRLE = 2;
    //COMPRESSION_CCITTFAX3 = COMPRESSION_CCITT_T4 = 3;
    //COMPRESSION_CCITTFAX4 = COMPRESSION_CCITT_T6 = 4;
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);

    std::string name = dest->GetName();
    if(! name.empty())
    {
        TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, name.c_str());
        TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, name.c_str());
    }
    if(page <= pages)
    {
        TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        TIFFSetField(tiff, TIFFTAG_PAGENUMBER, page, pages);
    }
    if(! dest->IsEmpty())
    {
        unsigned int sizel8 = (size[0]+7)/8;
        unsigned char* liner = new unsigned char[sizel8];
        int begin[TMazdaRoi::Dimensions];
        int end[TMazdaRoi::Dimensions];
        dest->GetBegin(begin);
        dest->GetEnd(end);
        end[0] = 0;
        y = 0;
        MazdaRoiRegionIterator<TMazdaRoi> iterator(dest, begin, end, true);
        do{
            SwapBitsInBytesInBuffer((unsigned char*) iterator.GetBlockPointer(), liner, sizel8);
            TIFFWriteScanline(tiff, (void*)liner, y, 0);
            ++iterator; y++;
        }while(!iterator.IsBehind());
        delete[] liner;

    }
    else
    {
        TIFFWriteScanline(tiff, (void*)(&zero), 0, 0);
    }
}


static bool ROIPagedTiffWriter(const char *filename, std::vector <TMazdaRoi*>* rois)
{
    unsigned int page;
    TIFF* tiff = TIFFOpen(filename, "w");
    if (!tiff) return false;
    for(page = 0; page < rois->size(); page++)
    {
        ROITiffWriter(tiff, (*rois)[page], page, rois->size());
        TIFFWriteDirectory(tiff);
    }
    TIFFClose(tiff);
    return true;
}

#endif // ROIIO_H
