//#include <opencv2/highgui/highgui_c.h>
//#include <opencv2/imgproc/imgproc_c.h>
//#include <opencv4/opencv2/imgproc/imgproc_c.h>
//#include <opencv4/opencv2/imgproc/imgproc_c.h>
#include <opencv2/imgproc.hpp>
#include <sane/sane.h>
#include <sane/saneopts.h>

#include <string>
#include <cstring>
#include <sstream>

extern double tlx;
extern double tly;
extern double brx;
extern double bry;


#ifdef __cplusplus
extern "C" {
#endif

static void
auth_callback (SANE_String_Const resource,
           SANE_Char * username, SANE_Char * password)
{
}
#ifdef __cplusplus
}
#endif

std::stringstream scannerMessageStream;
std::stringstream scannerMessageStreamLocal;

bool scannerInit(void)
{
    SANE_Int version_code = 0;
    SANE_Status status = sane_init(&version_code, auth_callback);
    if(status == SANE_STATUS_GOOD)
        return true;
    return false;
}
void scannerFinit(void)
{
    sane_exit();
}
cv::Mat scannerGet(void)
{
    cv::Mat return_result;
    scannerMessageStream.str(std::string());
    scannerMessageStreamLocal.str(std::string());
    const SANE_Device ** device_list = NULL;
    SANE_Status sane_status = SANE_STATUS_GOOD;
    sane_status = sane_get_devices (&device_list, SANE_FALSE);
    if (sane_status)
    {
        scannerMessageStream << "Scanner: " << sane_strstatus(sane_status) << std::endl;
        return return_result;
    }

    int devind = -1;
    bool scanned = false;
    while(device_list[devind+1] && !scanned)
    {
        devind ++;
        const SANE_Device *device = device_list[devind];
        if (!device)
        {
            scannerMessageStream << "Flatbed scanner not found" << std::endl;;
            return return_result;
        }
    //    scannerMessageStreamLocal << "Vendor: " << device->vendor <<", model: " << device->model<< ", type: " << device->type << std::endl;
        scannerMessageStream.str(std::string());
        scannerMessageStream << device->vendor << " " << device->model;
        scannerMessageStreamLocal << device->vendor << " " << device->model << std::endl;
        SANE_Handle sane_handle = NULL;
        sane_status = SANE_STATUS_GOOD;

        sane_status = sane_open(device->name, &sane_handle);
        if (sane_status)
        {
            scannerMessageStreamLocal << "Scanner communication: " << sane_strstatus(sane_status) << std::endl;
            continue;
        }
        int n = 0;
        bool resolutionset = false;
        const SANE_Option_Descriptor* sodescriptor;
        sodescriptor = sane_get_option_descriptor (sane_handle, n);
        sane_status = SANE_STATUS_GOOD;
        while(sodescriptor != NULL)
        {
            if(sodescriptor->size < 128 && sodescriptor->type == SANE_TYPE_INT && strcmp("resolution", sodescriptor->name) == 0)
            {
                SANE_Int dpi = 600;
                sane_status = sane_control_option(sane_handle, n, SANE_ACTION_SET_VALUE, (void*) &dpi, NULL);
                resolutionset = true;
            }
            if(sodescriptor->type == SANE_TYPE_FIXED && strcmp("tl-x", sodescriptor->name) == 0 && tlx >= 0.0)
            {
                SANE_Fixed co = SANE_FIX(tlx);
                sane_status = sane_control_option(sane_handle, n, SANE_ACTION_SET_VALUE, (void*) &co, NULL);
            }
            if(sodescriptor->type == SANE_TYPE_FIXED && strcmp("tl-y", sodescriptor->name) == 0 && tly >= 0.0)
            {
                SANE_Fixed co = SANE_FIX(tly);
                sane_status = sane_control_option(sane_handle, n, SANE_ACTION_SET_VALUE, (void*) &co, NULL);
            }
            if(sodescriptor->type == SANE_TYPE_FIXED && strcmp("br-x", sodescriptor->name) == 0 && brx >= 0.0)
            {
                SANE_Fixed co = SANE_FIX(brx);
                sane_status = sane_control_option(sane_handle, n, SANE_ACTION_SET_VALUE, (void*) &co, NULL);
            }
            if(sodescriptor->type == SANE_TYPE_FIXED && strcmp("br-y", sodescriptor->name) == 0 && bry >= 0.0)
            {
                SANE_Fixed co = SANE_FIX(bry);
                sane_status = sane_control_option(sane_handle, n, SANE_ACTION_SET_VALUE, (void*) &co, NULL);
            }
//            printf("%i: %s %i %i\n", n, sodescriptor->name, sodescriptor->type, sodescriptor->size);
//            fflush(stdout);
            n++;
            sodescriptor = sane_get_option_descriptor (sane_handle, n);
        }
        if(!resolutionset)
        {
            scannerMessageStreamLocal << "Setting 600DPI resolution: " << sane_strstatus(sane_status) << std::endl;
            sane_cancel(sane_handle);
            sane_close(sane_handle);
            continue;
        }
        sane_status = sane_start(sane_handle);
        if (sane_status)
        {
            scannerMessageStreamLocal << "Scanner start: " << sane_strstatus(sane_status) << std::endl;
            sane_cancel(sane_handle);
            sane_close(sane_handle);
            continue;
        }
        SANE_Parameters parm;
        sane_status = sane_get_parameters (sane_handle, &parm);
        if (sane_status)
        {
            scannerMessageStreamLocal << "Scanner parameters: " << sane_strstatus(sane_status) << std::endl;
            sane_cancel(sane_handle);
            sane_close(sane_handle);
            continue;
        }
        if(parm.depth == 8 && parm.format == SANE_FRAME_RGB)
        {
            unsigned int toload = parm.bytes_per_line * parm.lines * ((parm.format == SANE_FRAME_RGB || parm.format == SANE_FRAME_GRAY) ? 1:3);
            unsigned int ttoload = toload;
            SANE_Int len;
            SANE_Byte* buffer = (SANE_Byte*) malloc(toload);
            SANE_Byte* tbuffer = buffer;
            do
            {
                sane_status = sane_read (sane_handle, tbuffer, ttoload, &len);
                ttoload -= len;
                tbuffer += len;
            }
            while(ttoload > 0 && sane_status == SANE_STATUS_GOOD);

            if(ttoload <= 0 || sane_status == SANE_STATUS_EOF)
            {
                cv::Mat temp(parm.lines, parm.pixels_per_line, CV_8UC3, buffer);
                temp.copyTo(return_result);
                free(buffer);
            }
            else
            {
                scannerMessageStreamLocal << "Scanning: " << sane_strstatus(sane_status) << std::endl;
                free(buffer);
                sane_cancel(sane_handle);
                sane_close(sane_handle);
                continue;
            }
        }
        else
        {
            scannerMessageStreamLocal << "Incorrect parameters" << std::endl;
            sane_cancel(sane_handle);
            sane_close(sane_handle);
            continue;
        }
        sane_cancel(sane_handle);
        sane_close(sane_handle);
        scanned = true;
    }
    if(!scanned)
    {
        scannerMessageStream.str(scannerMessageStreamLocal.str());
    }
    return return_result;
}
