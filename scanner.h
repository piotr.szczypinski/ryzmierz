#ifndef SCANNER_H
#define SCANNER_H

#include <string>
#include <sstream>

#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>

extern std::stringstream scannerMessageStreamLocal;
extern std::stringstream scannerMessageStream;

//bool scannerGet(IplImage** pImage);
cv::Mat scannerGet(void);

bool scannerInit(void);

void scannerFinit(void);


//#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
//#include "scannerwia.cpp"
//#else
//#include "scanner.cpp"
//#endif

#endif // SCANNER_H
