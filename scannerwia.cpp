#include <Windows.h>
#include <objbase.h>
#include <stdio.h>
#include <tchar.h>
#include <Wia.h>
#include <OleAuto.h>
#include <Shlwapi.h>
#include <strsafe.h>

#include <scanner.h>

#define MAX_TEMP_PATH 2048
#define MAX_FILENAME_LENGTH 2048
#define REQUIRED_SCANNING_DPI 600

std::stringstream scannerMessageStreamLocal;
std::stringstream scannerMessageStream;

extern double tlx;
extern double tly;
extern double brx;
extern double bry;
extern int orientation;

class CWiaTransferCallback : public IWiaTransferCallback
{
private:
    ULONG m_cRef;
    BSTR  m_bstrFileExtension;           //file extension to be appended to the download file
    BSTR  m_bstrDirectoryName;           //download directory
    TCHAR m_szFileName[MAX_FILENAME_LENGTH];   //download file

public:
    // Constructor and destructor
    CWiaTransferCallback();
    virtual ~CWiaTransferCallback();

    HRESULT InitializeCallback(TCHAR* bstrDirectoryName, BSTR bstrExt);
    HRESULT CALLBACK QueryInterface( REFIID riid, void **ppvObject );
    ULONG CALLBACK AddRef();
    ULONG CALLBACK Release();
    HRESULT STDMETHODCALLTYPE TransferCallback(LONG lFlags,WiaTransferParams  *pWiaTransferParams);
    HRESULT STDMETHODCALLTYPE GetNextStream(LONG lFlags, BSTR bstrItemName, BSTR bstrFullItemName, IStream **ppDestination);
    const std::string getFileName(void);
};
CWiaTransferCallback::CWiaTransferCallback()
{
    m_cRef             = 1;  // initializing it to 1 so that when object is created, we can call Release() on it.
    m_bstrFileExtension = nullptr;
    m_bstrDirectoryName = nullptr;
    memset(m_szFileName, 0, sizeof(m_szFileName));
}
CWiaTransferCallback::~CWiaTransferCallback()
{
    if(m_bstrDirectoryName)
    {
        SysFreeString(m_bstrDirectoryName);
        m_bstrDirectoryName = nullptr;
    }
    if(m_bstrFileExtension)
    {
        SysFreeString(m_bstrFileExtension);
        m_bstrFileExtension = nullptr;
    }
}
HRESULT CWiaTransferCallback::InitializeCallback(TCHAR* bstrDirectoryName, BSTR bstrExt)
{
    HRESULT hr = S_OK;
    if(bstrDirectoryName)
    {
        m_bstrDirectoryName = SysAllocString(bstrDirectoryName);
        if(!m_bstrDirectoryName)
        {
            hr = E_OUTOFMEMORY;
            return hr;
        }
    }
    else
    {
        return E_INVALIDARG;
    }
    if (bstrExt)
    {
        m_bstrFileExtension = bstrExt;
    }
    return hr;
}
HRESULT CALLBACK CWiaTransferCallback::QueryInterface( REFIID riid, void **ppvObject )
{
    if (nullptr == ppvObject)
    {
        HRESULT hr = E_INVALIDARG;
        return hr;
    }
    if (IsEqualIID( riid, IID_IUnknown ))
    {
        *ppvObject = static_cast<IUnknown*>(this);
    }
    else if (IsEqualIID( riid, IID_IWiaTransferCallback ))
    {
        *ppvObject = static_cast<IWiaTransferCallback*>(this);
    }
    else
    {
        *ppvObject = nullptr;
        return (E_NOINTERFACE);
    }
    reinterpret_cast<IUnknown*>(*ppvObject)->AddRef();
    return S_OK;
}
ULONG CALLBACK CWiaTransferCallback::AddRef()
{
    return InterlockedIncrement((long*)&m_cRef);
}
ULONG CALLBACK CWiaTransferCallback::Release()
{
    LONG cRef = InterlockedDecrement((long*)&m_cRef);
    if (0 == cRef)
    {
        delete this;
    }
    return cRef;
}
HRESULT STDMETHODCALLTYPE CWiaTransferCallback::TransferCallback(LONG lFlags, WiaTransferParams* pWiaTransferParams)
{
//    static LONG lpc = 1000;
    HRESULT hr = S_OK;
    if(pWiaTransferParams == nullptr)
    {
        hr = E_INVALIDARG;
        return hr;
    }
//    switch (pWiaTransferParams->lMessage)
//    {
//        case WIA_TRANSFER_MSG_STATUS:
//            {
//                if(lpc != pWiaTransferParams->lPercentComplete)
//                {
//                    lpc = pWiaTransferParams->lPercentComplete;
//                    _tprintf(TEXT("\nWIA_TRANSFER_MSG_STATUS - %ld%% complete"), lpc);
//                }
//            }
//            break;
//        case WIA_TRANSFER_MSG_END_OF_STREAM:
//            {
//                _tprintf(TEXT("\nWIA_TRANSFER_MSG_END_OF_STREAM"));
//            }
//            break;
//        case WIA_TRANSFER_MSG_END_OF_TRANSFER:
//            {
//                _tprintf(TEXT("\nWIA_TRANSFER_MSG_END_OF_TRANSFER"));
//                _tprintf(TEXT("\nImage Transferred to file %ws"), m_szFileName);
//            }
//            break;
//        default:
//            break;
//    }
    return hr;
}
HRESULT STDMETHODCALLTYPE CWiaTransferCallback::GetNextStream(LONG lFlags, BSTR bstrItemName, BSTR bstrFullItemName, IStream **ppDestination)
{
    HRESULT hr = S_OK;
    if ( (!ppDestination) || (!bstrItemName) || (!m_bstrDirectoryName) )
    {
        hr = E_INVALIDARG;
        return hr;
    }
    *ppDestination = nullptr;
    if(m_bstrFileExtension)
    {
//        if (m_bFeederTransfer)
//        {
//            StringCchPrintf(m_szFileName, ARRAYSIZE(m_szFileName), TEXT("%ws\\%ws_page%d.%ws"), m_bstrDirectoryName, bstrItemName, ++m_lPageCount, m_bstrFileExtension);
//        }
//        else
//        {
            StringCchPrintf(m_szFileName, ARRAYSIZE(m_szFileName), TEXT("%ws\\%ws.%ws"), m_bstrDirectoryName, bstrItemName, m_bstrFileExtension);
//        }
    }
    else
    {
        StringCchPrintf(m_szFileName, ARRAYSIZE(m_szFileName), TEXT("%ws\\%ws"), m_bstrDirectoryName, bstrItemName);
    }
    hr = SHCreateStreamOnFile(m_szFileName, STGM_CREATE | STGM_READWRITE, ppDestination);
    if (SUCCEEDED(hr))
    {
        // We're not going to keep the Stream around, so don't AddRef.
        //  The caller will release the stream when done.
    }
    else
    {
        *ppDestination = nullptr;
        hr = E_FAIL;
    }
    return hr;
}
const std::string CWiaTransferCallback::getFileName(void)
{
    long length = GetShortPathNameW(m_szFileName, nullptr, 0);
    if (length == 0)
    {
        return std::string();
    }
    wchar_t* buffer = new wchar_t[length];
    GetShortPathNameW(m_szFileName, buffer, length);
    std::wstring ws = buffer;
    std::string rs(ws.begin(), ws.end());
    delete[] buffer;
    return rs;
}

bool downloadItem(IWiaItem2* pWiaItem2, IplImage** pImage)
{
    bool retval = false;
    IWiaTransfer *pWiaTransfer = nullptr;
    HRESULT hr = pWiaItem2->QueryInterface( IID_IWiaTransfer, (void**)&pWiaTransfer );
    if (SUCCEEDED(hr) && pWiaTransfer != nullptr)
    {
        CWiaTransferCallback *pWiaClassCallback = new CWiaTransferCallback;
        if (pWiaClassCallback)
        {
            IWiaTransferCallback *pWiaTransferCallback = nullptr;
            hr = pWiaClassCallback->QueryInterface( IID_IWiaTransferCallback, (void**)&pWiaTransferCallback );
            if (SUCCEEDED(hr) && pWiaTransferCallback != nullptr)
            {
                IWiaPropertyStorage* pWiaPropertyStorage = nullptr;
                HRESULT hr = pWiaItem2->QueryInterface( IID_IWiaPropertyStorage, (void**)&pWiaPropertyStorage );
                if(SUCCEEDED(hr) && pWiaPropertyStorage != nullptr)
                {
//                    GUID itemCategory = GUID_nullptr;
//                    readPropertyGuid(pWiaItem2, WIA_IPA_ITEM_CATEGORY, &itemCategory );
                    PROPSPEC PropSpecFlatbed[1] = {0};
                    PROPVARIANT PropVarFlatbed[1];
                    PropVariantInit(PropVarFlatbed);
                    PropSpecFlatbed[0].ulKind = PRSPEC_PROPID;
                    PropSpecFlatbed[0].propid = WIA_IPA_ITEM_CATEGORY;
                    hr = pWiaPropertyStorage->ReadMultiple(1, PropSpecFlatbed, PropVarFlatbed);
                    if(S_OK == hr && PropVarFlatbed[0].vt == VT_CLSID)
                    {
                        if(IsEqualIID(*PropVarFlatbed[0].puuid, WIA_CATEGORY_FLATBED))
                        {
                            GUID guid = WiaImgFmt_BMP;
                            PROPSPEC PropSpecImgFmt[1] = {0};
                            PROPVARIANT PropVarImgFmt[1];
                            PropVariantInit(PropVarImgFmt);
                            PropSpecImgFmt[0].ulKind = PRSPEC_PROPID;
                            PropSpecImgFmt[0].propid = WIA_IPA_FORMAT;
                            PropVarImgFmt[0].vt = VT_CLSID;
                            PropVarImgFmt[0].puuid = & guid;
                            hr = pWiaPropertyStorage->WriteMultiple(1, PropSpecImgFmt, PropVarImgFmt, WIA_IPA_FIRST);

                            if(SUCCEEDED(hr))
                            {
                                PROPSPEC PropSpecExt[1] = {0};
                                PROPVARIANT PropVarExt[1];
                                PropVariantInit(PropVarExt);
                                PropSpecExt[0].ulKind = PRSPEC_PROPID;
                                PropSpecExt[0].propid = WIA_IPA_FILENAME_EXTENSION;
                                HRESULT hr = pWiaPropertyStorage->ReadMultiple(1, PropSpecExt, PropVarExt);

                                TCHAR bufferTempPath[MAX_TEMP_PATH];
                                GetTempPath(MAX_TEMP_PATH , bufferTempPath);

                                LONG lItemType = 0;
                                hr = pWiaItem2->GetItemType( &lItemType );

                                PROPSPEC PropSpecResolution[2] = {0, 0};
                                PROPVARIANT PropVarResolution[2];
                                PropVariantInit(PropVarResolution);
                                PropSpecResolution[0].ulKind = PRSPEC_PROPID;
                                PropSpecResolution[0].propid = WIA_IPS_XRES;
                                PropVarResolution[0].vt = VT_I4;
                                PropVarResolution[0].lVal = REQUIRED_SCANNING_DPI;
                                PropSpecResolution[1].ulKind = PRSPEC_PROPID;
                                PropSpecResolution[1].propid = WIA_IPS_YRES;
                                PropVarResolution[1].vt = VT_I4;
                                PropVarResolution[1].lVal = REQUIRED_SCANNING_DPI;
                                hr = pWiaPropertyStorage->WriteMultiple(2, PropSpecResolution, PropVarResolution, WIA_IPA_FIRST);
                                PropVariantClear(PropVarResolution);

                                if(tlx >= 0 && tly >=0 && brx > tlx && bry > tly)
                                {
                                    PROPSPEC PropSpecRectangle[4] = {0, 0, 0, 0};
                                    PROPVARIANT PropVarRectangle[4];
                                    PropVariantInit(PropVarRectangle);
                                    PropSpecRectangle[0].ulKind = PRSPEC_PROPID;
                                    PropSpecRectangle[0].propid = WIA_IPS_XPOS;
                                    PropVarRectangle[0].vt = VT_I4;
                                    PropVarRectangle[0].lVal = (int)tlx;
                                    PropSpecRectangle[1].ulKind = PRSPEC_PROPID;
                                    PropSpecRectangle[1].propid = WIA_IPS_YPOS;
                                    PropVarRectangle[1].vt = VT_I4;
                                    PropVarRectangle[1].lVal = (int)tly;

                                    PropSpecRectangle[2].ulKind = PRSPEC_PROPID;
                                    PropSpecRectangle[2].propid = WIA_IPS_XEXTENT;
                                    PropVarRectangle[2].vt = VT_I4;
                                    PropVarRectangle[2].lVal = (int)(brx-tlx);
                                    PropSpecRectangle[3].ulKind = PRSPEC_PROPID;
                                    PropSpecRectangle[3].propid = WIA_IPS_YEXTENT;
                                    PropVarRectangle[3].vt = VT_I4;
                                    PropVarRectangle[3].lVal = (int)(bry-tly);
                                    pWiaPropertyStorage->WriteMultiple(4, PropSpecRectangle, PropVarRectangle, WIA_IPS_FIRST);
                                    PropVariantClear(PropVarRectangle);
                                }

                                if(orientation >= 0)
                                {
                                    PROPSPEC PropSpecOrientation[1] = {0};
                                    PROPVARIANT PropVarOrientation[1];
                                    PropVariantInit(PropVarOrientation);
                                    PropSpecOrientation[0].ulKind = PRSPEC_PROPID;
                                    PropSpecOrientation[0].propid = WIA_IPS_ORIENTATION;
                                    PropVarOrientation[0].vt = VT_I4;
                                    PropVarOrientation[0].lVal = (int)orientation;
                                    pWiaPropertyStorage->WriteMultiple(1, PropSpecOrientation, PropVarOrientation, WIA_IPS_FIRST);
                                    PropVariantClear(PropVarOrientation);
                                }

                                if(SUCCEEDED(hr))
                                {
                                    if(lItemType & WiaItemTypeStorage)
                                        pWiaClassCallback->InitializeCallback(bufferTempPath, nullptr);
                                    else
                                        pWiaClassCallback->InitializeCallback(bufferTempPath, PropVarExt[0].bstrVal);
                                    hr = pWiaTransfer->Download(0, pWiaTransferCallback);
                                    if(S_OK == hr)
                                    {
                                        //MessageBoxA(NULL, pWiaClassCallback->getFileName().c_str(), "Zapotoczny A3", MB_OK);
                                        *pImage = cvLoadImage(pWiaClassCallback->getFileName().c_str(), CV_LOAD_IMAGE_COLOR);
                                        retval = true;
                                    }
                                }
                                else
                                {
                                    scannerMessageStreamLocal << "Cannot set 600DPI resolution" << std::endl;
                                }
                                PropVariantClear(PropVarExt);
                                pWiaPropertyStorage->Release();
                            }
                        }
                    }
                    PropVariantClear(PropVarFlatbed);
                }
                pWiaTransferCallback->Release();
            }
            pWiaClassCallback->Release();
        }
        pWiaTransfer->Release();
    }
    return retval;
}

bool scannerGet(IplImage** pImage)
{
    scannerMessageStream.str(std::string());
    scannerMessageStreamLocal.str(std::string());
    bool retval = false;
    IWiaDevMgr2 *pWiaDevMgr2 = nullptr;
    HRESULT hr = CoCreateInstance( CLSID_WiaDevMgr2, nullptr, CLSCTX_LOCAL_SERVER, IID_IWiaDevMgr2, (void**)&pWiaDevMgr2 );
    if (SUCCEEDED(hr) && pWiaDevMgr2 != nullptr)
    {
        IEnumWIA_DEV_INFO *pWiaEnumDevInfo = nullptr;
        HRESULT hr = pWiaDevMgr2->EnumDeviceInfo( WIA_DEVINFO_ENUM_LOCAL, &pWiaEnumDevInfo );
        if (SUCCEEDED(hr))
        {
            hr = pWiaEnumDevInfo->Reset();
            if (SUCCEEDED(hr))
            {
                //Bez while bierze pierwszy napotkany skaner
                while (S_OK == hr && !retval)
                {
                    IWiaPropertyStorage *pWiaPropertyStorage = nullptr;
                    hr = pWiaEnumDevInfo->Next( 1, &pWiaPropertyStorage, nullptr );
                    if (hr == S_OK && pWiaPropertyStorage != nullptr)
                    {
                        PROPSPEC PropSpec[2] = {0, 0};
                        PROPVARIANT PropVar[2];
                        PropVariantInit(PropVar);
                        PropSpec[0].ulKind = PRSPEC_PROPID;
                        PropSpec[0].propid = WIA_DIP_DEV_ID;
                        PropSpec[1].ulKind = PRSPEC_PROPID;
                        PropSpec[1].propid = WIA_DIP_DEV_NAME;
                        HRESULT hr = pWiaPropertyStorage->ReadMultiple(2, PropSpec, PropVar);
                        if(S_OK == hr)
                        {
                            std::wstring ws = PropVar[1].bstrVal;
                            std::string rs(ws.begin(), ws.end());

                            scannerMessageStreamLocal << rs << std::endl;
                            scannerMessageStream << rs;
//                            if(PropVar[0].vt == VT_BSTR)
//                                _tprintf( TEXT("WIA_DIP_DEV_ID: %ws\n"), PropVar[0].bstrVal );
//                            if(PropVar[0].vt == VT_BSTR)
//                                _tprintf( TEXT("WIA_DIP_DEV_NAME: %ws\n"), PropVar[1].bstrVal );
                            IWiaItem2 *pWiaRootItem2 = nullptr;
                            hr = pWiaDevMgr2->CreateDevice( 0, PropVar[0].bstrVal, &pWiaRootItem2 );
                            if(SUCCEEDED(hr) && pWiaRootItem2 != nullptr)
                            {
                                IEnumWiaItem2 *pEnumWiaItem2 = nullptr;
                                HRESULT hr = pWiaRootItem2->EnumChildItems( nullptr, &pEnumWiaItem2 );  // Powinien szukac FLATBED
                                if (SUCCEEDED(hr) && pEnumWiaItem2 != nullptr)
                                {
                                    while (S_OK == hr && !retval)
                                    {
                                        IWiaItem2 *pChildWiaItem2 = nullptr;
                                        hr = pEnumWiaItem2->Next(1, &pChildWiaItem2, nullptr);
                                        if(S_OK == hr && pChildWiaItem2 != nullptr)
                                        {
                                            LONG lItemType = 0;
                                            HRESULT hr = pChildWiaItem2->GetItemType( &lItemType );
                                            if((lItemType & WiaItemTypeTransfer) && (lItemType & WiaItemTypeFile))
                                            {
                                                retval = downloadItem(pChildWiaItem2, pImage);
                                            }
                                            pChildWiaItem2->Release();
                                            pChildWiaItem2 = nullptr;
                                        }
                                    }
                                    pEnumWiaItem2->Release();
                                    pEnumWiaItem2 = nullptr;
                                }
                                pWiaRootItem2->Release();
                                pWiaRootItem2 = nullptr;
                            }
                        }
                        else
                        {
                            scannerMessageStreamLocal << "Flatbed scanner not found" << std::endl;
                        }
                        PropVariantClear(PropVar);
                        pWiaPropertyStorage->Release();
                        pWiaPropertyStorage = nullptr;
                    }
                }
            }
            pWiaEnumDevInfo->Release();
            pWiaEnumDevInfo = nullptr;
        }
        pWiaDevMgr2->Release();
        pWiaDevMgr2 = nullptr;
    }
    if(!retval)
    {
        scannerMessageStream.str(scannerMessageStreamLocal.str());
    }
    return retval;
}

bool scannerInit()
{
    HRESULT hr = CoInitialize(nullptr);
    if (SUCCEEDED(hr))
        return true;
    return false;
}
void scannerFinit()
{
    CoUninitialize();
}
