#ifndef SERVICECONFIG_H
#define SERVICECONFIG_H

#include <QDialog>
#include <QJsonObject>


namespace Ui {
class ServiceConfig;
}

struct SftpConfig
{
    QString hostaddr;
    unsigned int port;
    QString username;
    QString password;
    QString sftppath;
};


class ServiceConfig : public QDialog
{
    Q_OBJECT

public:
    explicit ServiceConfig(QWidget *parent = 0);
    ~ServiceConfig();
    SftpConfig getSftpConfig(void);
    void setSftpConfig(SftpConfig config);

private slots:

private:
    Ui::ServiceConfig *ui;
};

void sftpJsonWriteSftpOptions(QJsonObject &json, const SftpConfig* config);
void sftpJsonReadSftpOptions(const QJsonObject &json, SftpConfig* config);
//bool serviceConfigLoadFromFile(SftpConfig* config, QString* filename);
//bool serviceConfigSaveToFile(SftpConfig* config, QString* filename);
bool scramblePassword(SftpConfig* config);
bool unscramblePassword(SftpConfig* config);
int sendToSftp(SftpConfig* config, QString* filename);


#endif // SERVICECONFIG_H
