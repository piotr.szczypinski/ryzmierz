<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>RyżMierz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="64"/>
        <source>Close all</source>
        <translation type="unfinished">Zamknij wszystko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>Scan</source>
        <translation type="unfinished">Skanuj</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <location filename="mainwindow.ui" line="806"/>
        <source>Upload</source>
        <translation type="unfinished">Wyślij do chmury</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Plik</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <source>He&amp;lp</source>
        <translation type="unfinished">&amp;Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="222"/>
        <source>Ed&amp;it</source>
        <translation type="unfinished">&amp;Edytuj</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="241"/>
        <source>&amp;Report</source>
        <translation type="unfinished">&amp;Raport</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Data</source>
        <translation type="unfinished">Dane</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="mainwindow.ui" line="793"/>
        <source>Property</source>
        <translation type="unfinished">Nazwa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="334"/>
        <location filename="mainwindow.ui" line="798"/>
        <source>Value</source>
        <translation type="unfinished">Wartość</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="342"/>
        <source>Comments</source>
        <translation type="unfinished">Uwagi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="382"/>
        <source>Mesurements</source>
        <translation type="unfinished">Pomiar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <source>Find kernels</source>
        <translation type="unfinished">Znajdź ziarna</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="453"/>
        <source>Reference length</source>
        <translation type="unfinished">Długość odniesienia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="501"/>
        <source>Compute</source>
        <translation type="unfinished">Oblicz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>Image</source>
        <translation type="unfinished">Obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="556"/>
        <source>Length</source>
        <translation type="unfinished">Długość</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="561"/>
        <source>Width</source>
        <translation type="unfinished">Szerokość</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="566"/>
        <source>Area</source>
        <translation type="unfinished">Powierzchnia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <source>Elongation</source>
        <translation type="unfinished">Wydłużenie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="576"/>
        <source>Concavity</source>
        <translation type="unfinished">Wklęsłość</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="616"/>
        <location filename="mainwindow.ui" line="742"/>
        <source>Print</source>
        <translation type="unfinished">Drukuj</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="mainwindow.ui" line="752"/>
        <source>Save</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <source>Plots</source>
        <translation type="unfinished">Wykresy</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="702"/>
        <source>Pieces</source>
        <translation type="unfinished">W sztukach</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="709"/>
        <source>Estimated weight</source>
        <translation type="unfinished">W estymowanej wadze</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="771"/>
        <source>Storage</source>
        <translation type="unfinished">Zapis wyników</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="830"/>
        <location filename="mainwindow.cpp" line="941"/>
        <source>Save images</source>
        <translation type="unfinished">Zapisz obrazy</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="841"/>
        <location filename="mainwindow.cpp" line="1229"/>
        <source>Save report</source>
        <translation type="unfinished">Zapisz raport</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="852"/>
        <location filename="mainwindow.cpp" line="1276"/>
        <source>Print report</source>
        <translation type="unfinished">Drukuj raport</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="873"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">Za&amp;kończ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="882"/>
        <source>&amp;About...</source>
        <translation type="unfinished">&amp;O programie...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <source>&amp;Find kernels</source>
        <translation type="unfinished">&amp;Znajdź ziarna</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="894"/>
        <source>Identifies kernels</source>
        <translation type="unfinished">Identyfikuje ziarna</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="897"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="906"/>
        <source>&amp;Import image...</source>
        <translation type="unfinished">&amp;Importuj obraz...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <source>Opens image file</source>
        <translation type="unfinished">Otwiera plik z obrazem</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="918"/>
        <source>S&amp;ave report...</source>
        <translation type="unfinished">Z&amp;apisz raport...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="921"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="930"/>
        <source>&amp;Print report...</source>
        <translation type="unfinished">Wy&amp;drukuj raport...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="933"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="942"/>
        <source>&amp;Remove item</source>
        <translation type="unfinished">&amp;Usuń element</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="945"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="954"/>
        <source>&amp;Compute reference length</source>
        <translation type="unfinished">&amp;Oblicz długość odniesienia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="957"/>
        <source>Compute reference length</source>
        <translation type="unfinished">Oblicz długość odniesienia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="960"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="969"/>
        <source>S&amp;can...</source>
        <translation type="unfinished">S&amp;kanuj...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="972"/>
        <source>Acquire from scannner</source>
        <translation type="unfinished">Pobierz ze skanera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="975"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="980"/>
        <source>&amp;Tst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="989"/>
        <source>&amp;Service configuration...</source>
        <translation type="unfinished">&amp;Konfiguracja serwisowa...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="998"/>
        <source>C&amp;lose all</source>
        <translation type="unfinished">&amp;Zamknij wszystko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1001"/>
        <source>Close all images and clear report </source>
        <translation type="unfinished">Zamyka wszystkie obrazy i czyści dane wynikowe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1010"/>
        <source>Sa&amp;ve images...</source>
        <translation type="unfinished">&amp;Zapisz obrazy...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1013"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1022"/>
        <source>L&amp;oad images...</source>
        <translation type="unfinished">&amp;Otwórz obrazy...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1025"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1034"/>
        <source>&amp;Upload to server</source>
        <translation type="unfinished">Wyślij do &amp;chmury</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>Expired version</source>
        <translation type="unfinished">Wersja wygasła</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>This version of RyżMierz have expired and requires an update</source>
        <translation type="unfinished">Ta wersja programu jest już nieaktualna i wymaga aktualizacji</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Length uniformity %1%</source>
        <translation type="unfinished">Wyrównanie długości %1%</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <location filename="mainwindow.cpp" line="630"/>
        <location filename="mainwindow.cpp" line="1467"/>
        <source>Length classes</source>
        <translation type="unfinished">Klasy długości</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="610"/>
        <location filename="mainwindow.cpp" line="646"/>
        <location filename="mainwindow.cpp" line="1469"/>
        <source>Roundness classes</source>
        <translation type="unfinished">Klasy wydłużenia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="625"/>
        <location filename="mainwindow.cpp" line="1465"/>
        <source>Length uniformity</source>
        <translation type="unfinished">Wyrównanie długości</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="830"/>
        <source>Options loaded</source>
        <translation type="unfinished">Załadowano opcje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="836"/>
        <source>Image loaded</source>
        <translation type="unfinished">Załadowano obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="902"/>
        <location filename="mainwindow.cpp" line="1023"/>
        <source>Unknown exception</source>
        <translation type="unfinished">Nieokreślony wyjątek</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="932"/>
        <source>Error saving images</source>
        <translation type="unfinished">Błąd zapisu obrazów</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <location filename="mainwindow.cpp" line="983"/>
        <source>Multi-page tagged image file format (*.tiff) (*.tiff)</source>
        <translation type="unfinished">Wieloobrazowy plik TIFF (*.tiff) (*.tiff)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="973"/>
        <source>Error loading images</source>
        <translation type="unfinished">Błąd odczytu obrazów</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="981"/>
        <source>Load images</source>
        <translation type="unfinished">Otwórz obrazy</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1009"/>
        <source>Import succeeded</source>
        <translation type="unfinished">Zaimportowano obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1014"/>
        <source>Cannot import</source>
        <translation type="unfinished">Błąd importowania obrazu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1033"/>
        <source>Import image</source>
        <translation type="unfinished">Importuj obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1035"/>
        <source>Image files (*.tif *.tiff *.bmp *.png *.jpg *.jpeg);;All files (*)</source>
        <translation type="unfinished">Formaty plików obrazu (*.tif *.tiff *.bmp *.png *.jpg *.jpeg);;Pokaż wszystkie (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1077"/>
        <source>Warning: Open image first</source>
        <translation type="unfinished">Ostrzeżenie: najpierw załaduj lub zeskanuj obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1079"/>
        <source>Warning: Cannot find objects</source>
        <translation type="unfinished">Ostrzeżenie: Nie mozna zidentyfikować ziaren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1081"/>
        <source>Warning: Cannot separate clusters</source>
        <translation type="unfinished">Ostrzeżenie: Nie można rozdzielić ziaren w skupiskach</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1231"/>
        <source>Portable Document (*.pdf) (*.pdf);;Open Document (*.odt) (*.odt);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1355"/>
        <source>Report</source>
        <translation type="unfinished">Raport</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1369"/>
        <source>Reference length %1mm default or entered manually</source>
        <translation type="unfinished">Długość odniesienia %1mm domyślna lub wpisana ręcznie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1374"/>
        <source>Reference length %1mm averaged earlier for %2 kernels</source>
        <translation type="unfinished">Długość odniesienia %1mm uśredniona dla poprzedniej partii %2 ziaren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <location filename="mainwindow.cpp" line="1383"/>
        <source>Reference length %1mm averaged for %2 kernels</source>
        <translation type="unfinished">Długość odniesienia %1mm uśredniona dla %2 ziaren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1404"/>
        <source>Imported image: %1</source>
        <translation type="unfinished">Zaimportowano obraz: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1417"/>
        <source>Comments: </source>
        <translation type="unfinished">Uwagi: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1447"/>
        <source>Contribution in estimated weight</source>
        <translation type="unfinished">Estymowany udział wagowy</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1454"/>
        <source>Contribution in pieces</source>
        <translation type="unfinished">Udział w liczbie sztuk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1733"/>
        <source>yyyy-MM-dd-hh-mm-ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1870"/>
        <source>Number of selected items: %1</source>
        <translation type="unfinished">Liczba znalezionych elementów: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="243"/>
        <source>small broken kernel</source>
        <translation type="unfinished">okruchy</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="244"/>
        <source>medium broken kernel</source>
        <translation type="unfinished">połamany</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="245"/>
        <source>large broken kernel</source>
        <translation type="unfinished">nadłamany</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="246"/>
        <source>head rice</source>
        <translation type="unfinished">cały</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="247"/>
        <source>whole kernel</source>
        <translation type="unfinished">całe ziarna</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="260"/>
        <source>round</source>
        <translation type="unfinished">okrągły</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="261"/>
        <source>bold</source>
        <translation type="unfinished">pogrubiony</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="262"/>
        <source>medium</source>
        <translation type="unfinished">średni</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="263"/>
        <source>slender</source>
        <translation type="unfinished">smukły</translation>
    </message>
</context>
<context>
    <name>ServiceConfig</name>
    <message>
        <location filename="serviceconfig.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="serviceconfig.ui" line="24"/>
        <source>Property</source>
        <translation type="unfinished">Nazwa</translation>
    </message>
    <message>
        <location filename="serviceconfig.ui" line="29"/>
        <source>Value</source>
        <translation type="unfinished">Wartość</translation>
    </message>
</context>
</TS>
