#include "serviceconfig.h"
#include "ui_serviceconfig.h"
#include "filenameremap.h"

//#define HAVE_WINSOCK2_H
//#define HAVE_SYS_SOCKET_H
//#define HAVE_UNISTD_H
//#define HAVE_NETINET_IN_H
//#define HAVE_ARPA_INET_H
//#include "libssh2_config.h"
#include <libssh2.h>
#include <libssh2_sftp.h>

#ifdef HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#include <QCryptographicHash>
#include <QFile>
#include <QTextStream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>

#include <time.h>
#include "paramtreewidget.h"


ServiceConfig::ServiceConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServiceConfig)
{
    ui->setupUi(this);
}

ServiceConfig::~ServiceConfig()
{
    delete ui;
}

void ServiceConfig::setSftpConfig(SftpConfig config)
{
    QTreeWidgetItem* item = NULL;
    ParameterTreeWidget* tree = ui->treeWidget_service;
    tree->createProperty(item, "Username", QString("t;%1").arg(config.username), "Username on the sftp server");
    tree->createProperty(item, "Password", QString("p;%1").arg(config.password), "Password");
    tree->createProperty(item, "Server", QString("t;%1").arg(config.hostaddr), "Internet address (IP) of the sftp server");
    tree->createProperty(item, "Port", QString("i;%1;0;65535").arg(config.port), "Sftp port");
    tree->createProperty(item, "Path", QString("t;%1").arg(config.sftppath), "Folder for data storage on sftp server");
    tree->expandAll();
}

SftpConfig ServiceConfig::getSftpConfig(void)
{
    SftpConfig config;
    QTreeWidgetItem *item;
    ParameterTreeWidget* tree = ui->treeWidget_service;
    item = tree->topLevelItem(0);
    if(item != NULL)
        config.username = item->text(1);
    item = tree->topLevelItem(1);
    if(item != NULL)
        config.password = item->text(1);
    item = tree->topLevelItem(2);
    if(item != NULL)
        config.hostaddr = item->text(1);
    item = tree->topLevelItem(3);
    if(item != NULL)
        config.port = item->text(1).toInt();
    item = tree->topLevelItem(4);
    if(item != NULL)
        config.sftppath = item->text(1);
    return config;
}

void sftpJsonWriteSftpOptions(QJsonObject &json, const SftpConfig* config)
{
    json["username"] = config->username;
    json["password"] = config->password;
    json["hostaddr"] = config->hostaddr;
    json["port"] = (int) config->port;
    json["path"] = config->sftppath;
}

void sftpJsonReadSftpOptions(const QJsonObject &json, SftpConfig* config)
{
    if(json.contains("username"))
        config->username = json["username"].toString();
    if(json.contains("password"))
        config->password = json["password"].toString();
    if(json.contains("hostaddr"))
        config->hostaddr = json["hostaddr"].toString();
    if(json.contains("port"))
        config->port = json["port"].toInt();
    if(json.contains("path"))
        config->sftppath = json["path"].toString();
}


//bool serviceConfigLoadFromFile(SftpConfig* config, QString* filename)
//{
//    std::string name;
//    std::ifstream file;
//    file.open(fileNameRemapRead(filename).c_str());
//    if (!file.is_open()) return false;
//    if (!file.good()) return false;
//    do
//    {
//        std::string sline;
//        std::getline(file, sline);
//        std::stringstream ss(sline);
//        while(!ss.eof())
//        {
//            name.clear();
//            ss >> std::skipws >> name;
//            if (name.compare("username") == 0)
//            {
//                ss >> std::skipws >> name;
//                config->username = QString(name.c_str());
//            }
//            else if (name.compare("password") == 0)
//            {
//                ss >> std::skipws >> name;
//                config->password = QString(name.c_str());
//            }
//            else if (name.compare("hostaddr") == 0)
//            {
//                ss >> std::skipws >> name;
//                config->hostaddr = QString(name.c_str()); //inet_addr(name.c_str());
//            }
//            else if (name.compare("port") == 0)
//            {
//                ss >> std::skipws >> config->port;
//            }
//            else if (name.compare("sftppath") == 0)
//            {
//                ss >> std::skipws >> name;
//                config->sftppath = QString(name.c_str());
//            }
//        }
//    }
//    while(!file.eof());
//    file.close();
//    return true;
//}

//bool serviceConfigSaveToFile(SftpConfig* config, QString* filename)
//{
//    QFile f;
//    f.setFileName(*filename);
//    if(f.open(QFile::WriteOnly))
//    {
//        QTextStream textstream(&f);
//        textstream << "username " << config->username << endl;
//        textstream << "password " << config->password << endl;
//        textstream << "hostaddr " << config->hostaddr << endl;
//        textstream << "port " << config->port << endl;
//        textstream << "sftppath " << config->sftppath << endl;
//        f.close();
//        return true;
//    }
//    else return false;
//}


bool scramblePassword(SftpConfig* config)
{
    srand (time(NULL));
    unsigned char randomchar = rand();
    QByteArray pass = config->password.toUtf8();
    QByteArray user = config->username.toUtf8();
    QByteArray hash = QCryptographicHash::hash(user, QCryptographicHash::Sha512);

    unsigned int pass_count = pass.count();
    unsigned int hash_count = hash.count();
    if(pass_count < hash_count)
    {
        hash[0] = hash[0] + randomchar;
        unsigned int i = 1;
        for(; i <= pass_count; i++)
        {
            hash[i] = (unsigned char)(hash[i-1] + pass[i-1] + hash[i]);
        }
        for(; i < hash_count; i++)
        {
            hash[i] = (unsigned char)(hash[i-1] + hash[i]);
        }
        QByteArray hex = hash.toHex();
        config->password = hex;
    }
    else
        return false;

    return true;
}

bool unscramblePassword(SftpConfig* config)
{
    QByteArray user = config->username.toUtf8();
    QByteArray hash = QCryptographicHash::hash(user, QCryptographicHash::Sha512);
    QByteArray pass = QByteArray::fromHex(config->password.toLatin1());

    unsigned int pass_count = pass.count();
    unsigned int hash_count = hash.count();
    if(pass_count == hash_count)
    {
        unsigned int i = 1;
        //pass[i] = pass[i] - hash[i];
        for(; i < pass_count; i++)
        {
            pass[i-1] = (unsigned char)(pass[i] - pass[i-1] - hash[i]);
        }
        pass[i-1] = 0;
        config->password = QString(pass);
    }
    else
        return false;

    return true;
}

int sendToSftp(SftpConfig* config, QString *filename)
{
    int ret = 0;
    int sock;
    struct sockaddr_in sin;
    unsigned long hostaddr;
    const char *fingerprint;
    LIBSSH2_SESSION *session;
    int rc;
    FILE *local;
    LIBSSH2_SFTP *sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    char mem[1024*100];
    size_t nread;
    char *ptr;

#ifdef WIN32
    WSADATA wsadata;
    int err;

    err = WSAStartup(MAKEWORD(2, 0), &wsadata);
    if(err != 0) {
        fprintf(stderr, "WSAStartup failed with error: %d\n", err);
        return 1;
    }
#endif
    hostaddr = inet_addr(config->hostaddr.toStdString().c_str()); //inet_ntoa(?)

    rc = libssh2_init(0);
    if(rc != 0)
        return 1;

    local = fopen(fileNameRemapRead(filename).c_str(), "rb");
    if(!local)
        return 2;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(config->port);
    sin.sin_addr.s_addr = hostaddr;
    if(connect(sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) != 0)
        return 3;

    session = libssh2_session_init();
    if(!session)
        return 4;

    libssh2_session_set_blocking(session, 1);
    rc = libssh2_session_handshake(session, sock);
    if(rc)
        return 5;

    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);

    if(libssh2_userauth_password(session, config->username.toStdString().c_str(), config->password.toStdString().c_str()))
    {
        ret = 6;
        goto shutdown;
    }
    sftp_session = libssh2_sftp_init(session);
    if(!sftp_session)
    {
        ret = 7;
        goto shutdown;
    }
    sftp_handle =
        libssh2_sftp_open(sftp_session, config->sftppath.toStdString().c_str(),
                      LIBSSH2_FXF_WRITE|LIBSSH2_FXF_CREAT|LIBSSH2_FXF_TRUNC,
                      LIBSSH2_SFTP_S_IRUSR|LIBSSH2_SFTP_S_IWUSR|
                      LIBSSH2_SFTP_S_IRGRP|LIBSSH2_SFTP_S_IROTH);
    if(!sftp_handle)
    {
        ret = 8;
        goto shutdown;
    }
    do
    {
        nread = fread(mem, 1, sizeof(mem), local);
        if(nread <= 0)
            break;
        ptr = mem;
        do
        {
            rc = libssh2_sftp_write(sftp_handle, ptr, nread);
            if(rc < 0)
                break;
            ptr += rc;
            nread -= rc;
        }
        while(nread);
    }
    while(rc > 0);
    libssh2_sftp_close(sftp_handle);
    libssh2_sftp_shutdown(sftp_session);

shutdown:
    libssh2_session_disconnect(session, "");
    libssh2_session_free(session);

#ifdef WIN32
    closesocket(sock);
#else
    close(sock);
#endif
    if(local)
        fclose(local);
    libssh2_exit();
    return ret;
}
