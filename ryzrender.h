#ifndef RYZRENDER_H
#define RYZRENDER_H

#include <QWidget>
#include "opencv2/imgproc.hpp"

class RyzRender : public QWidget
{
    Q_OBJECT
public:
    explicit RyzRender(QWidget *parent = nullptr);
    ~RyzRender();
    void paintEvent(QPaintEvent *e);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

    void setZoom(double zoom);
    double getZoom(void)
    {
        return screen_zoom;
    }
    void centerAt(int x, int y);
    void setImage(cv::Mat& src);
    void setContours(std::vector<std::vector<cv::Point>>& contours);

    int getContourIndex(const double x, const double y);
    int getContoursCount(void)
    {
        return contours.size();
    }
    std::vector<std::vector<cv::Point>> getContours(void)
    {
        return contours;
    }
    cv::Mat getImage(void)
    {
        return image;
    }
    void repaintContour(const int i);
    QPixmap getPixmap(const std::vector<cv::Point>& cr, const int decimate);
    //QPixmap getPixmap(std::vector<std::vector<cv::Point>>& cr, const int decimate);
    QPixmap getPixmap(const int i, const int decimate);
    bool imported;
signals:
    void mousePointed(double x, double y);
    void mouseReleased(void);
    void zoomChanged(double);

public slots:

private:
    void zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom);
    cv::Mat image;
    std::vector<std::vector<cv::Point>> contours;
    //int contourCount;
    int max_contour_distance;
    
    cv::Mat screen;
    QImage screen_image;
    double screen_zoom;

};

#endif // RYZRENDER_H
