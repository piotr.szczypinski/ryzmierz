/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QHBoxLayout>
#include <QDate>
#include <QDateEdit>
#include <QLineEdit>
#include <QTreeWidget>
#include <QStyledItemDelegate>
#include "paramtreewidget.h"


// ---------------------------- UWAGA ----------------------------
// Jeśli są błędy "undefined reference to vtable for"
// dodaj do pliku projektowego plik nagłówkowy "paramtreewidget.h"
// ---------------------------- UWAGA ----------------------------



/**
 * @brief ParameterTreeWidget::ParameterTreeWidget constructor of tree widget
 * @param parent
 */
ParameterTreeWidget::ParameterTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
    setItemDelegateForColumn(0, new NoEditDelegate(parent));
    setItemDelegateForColumn(1, new EditDelegate(parent, this));
}

/**
 * @brief ParameterTreeWidget::createMethod creates method (top level item)
 * @param name
 * @param tooltip
 * @return pointer to the created item
 */
QTreeWidgetItem* ParameterTreeWidget::createMethod(const QString name, const QString tooltip)
{
    QTreeWidgetItem* methoditem = new QTreeWidgetItem();
    methoditem->setText(0, name);
    methoditem->setToolTip(0, tooltip);
    addTopLevelItem(methoditem);
    return methoditem;
}

/**
 * @brief ParameterTreeWidget::createProperty creates child item (non top level item)
 * @param parent
 * @param name
 * @param value
 * @param tooltip
 * @return
 */
QTreeWidgetItem* ParameterTreeWidget::createProperty(QTreeWidgetItem* parent, const QString name, const QString value, const QString tooltip)
{
    QTreeWidgetItem* parameteritem = new QTreeWidgetItem();
    int column = 0;
    if(! name.isEmpty())
    {
        parameteritem->setToolTip(column, tooltip);
        parameteritem->setText(column, name);
        parameteritem->setToolTip(column, tooltip);
        column++;
    }
    if(! value.isEmpty())
    {
        parameteritem->setToolTip(column, tooltip);
        parameteritem->setData(column, Qt::UserRole, QString(value));
        QStringList valist = value.split(";");
        if(valist.size() > 1)
        {
            if(valist[0] == "D" && valist[1] == "?")
            {
                QDateEdit qde;
                qde.setDate(QDate::currentDate());
                parameteritem->setText(column, qde.text());
            }
            else if(valist[0] == "d" && valist[1] == "?")
            {
                parameteritem->setText(column, "");
            }
            else if(valist[0] == "p" || valist[0] == "P")
            {
                parameteritem->setText(column, QString(valist[1].length(), QChar(0x25CF)));
            }
            else
                parameteritem->setText(column, valist[1]);
        }
        else
            parameteritem->setText(column, value);
    }
    parameteritem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable);
    if(parent != NULL) parent->addChild(parameteritem);
    else addTopLevelItem(parameteritem);
    return parameteritem;
}

/**
 * @brief NoEditDelegate::NoEditDelegate delegate constructor for non editable items
 * @param parent
 */
NoEditDelegate::NoEditDelegate(QObject* parent) : QStyledItemDelegate(parent)
{
}

/**
 * @brief NoEditDelegate::createEditor creates null editor for non editable items
 * @return
 */
QWidget* NoEditDelegate::createEditor(QWidget*, const QStyleOptionViewItem &, const QModelIndex &) const
{
    return 0;
}


/**
 * @brief EditDelegate::EditDelegate delegate constructor for editable items
 * @param parent
 * @param tree
 * @param mergeSeparator
 */
EditDelegate::EditDelegate(QObject *parent, ParameterTreeWidget* tree/*, char mergeSeparator*/)
    : QStyledItemDelegate(parent)
{
    this->tree = tree;
}

#define passEchoMode QLineEdit::PasswordEchoOnEdit
//#define passEchoMode QLineEdit::Password
//#define passEchoMode QLineEdit::Normal

/**
 * @brief EditDelegate::createEditor creates editors: combo or spin boxes
 * @param parent
 * @param option
 * @param index
 * @return
 */
QWidget* EditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStringList valist = index.model()->data(index, Qt::UserRole).toString().split(";");
    if(valist.size() <= 0)
        return NULL;
    if(valist[0].length() <= 0)
        return NULL;
    if(valist[0][0].isUpper())
        return NULL;

    if(valist.size() >= 1)
    {
        if(valist[0] == "d")
        {
            QDateEdit *editor = new QDateEdit(parent);
            editor->setFrame(false);
            return editor;
        }
        else if(valist[0] == "i")
        {
            QSpinBox *editor = new QSpinBox(parent);
            editor->setFrame(false);
            if(valist.size() >= 4)
            {
                editor->setMinimum(valist[2].toInt());
                editor->setMaximum(valist[3].toInt());
            }
            return editor;
        }
        else if(valist[0] == "f")
        {
            QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
            editor->setDecimals(3);
            editor->setFrame(false);
            if(valist.size() >= 4)
            {
                editor->setMinimum(valist[2].toDouble());
                editor->setMaximum(valist[3].toDouble());
            }
            return editor;
        }
        else if(valist[0] == "t")
        {
            if(valist.size() >= 4)
            {
                QComboBox *editor = new QComboBox(parent);
                editor->setFrame(false);
                editor->setAutoFillBackground(true);
                for(int i = 2; i < valist.size(); i++) editor->addItem(valist[i]);
                return editor;
            }
        }
        else if(valist[0] == "p")
        {
            QLineEdit *editor = new QLineEdit(parent);
            editor->setFrame(false);
            editor->setEchoMode(passEchoMode);
            return editor;
        }
    }
    return QStyledItemDelegate::createEditor(parent, option, index);
}

/**
 * @brief EditDelegate::setEditorData sets current data for editor
 * @param editor
 * @param index
 */
void EditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QSpinBox *spinBox = qobject_cast <QSpinBox *>(editor);
    QComboBox *comboBox = qobject_cast <QComboBox *>(editor);
    QDoubleSpinBox *doubleSpinBox = qobject_cast <QDoubleSpinBox *>(editor);
//    QDateEdit *dateEdit = qobject_cast <QDateEdit *>(editor);
    QLineEdit *lineEdit = qobject_cast <QLineEdit *>(editor);

    if(lineEdit != NULL)
        if(lineEdit->echoMode() != passEchoMode)
            lineEdit = NULL;

    if(spinBox != NULL)
    {
        int value;
        value = index.model()->data(index, Qt::EditRole).toInt();
        spinBox->setValue(value);
    }
    else if(doubleSpinBox != NULL)
    {
        double value;
        value = index.model()->data(index, Qt::EditRole).toDouble();
        doubleSpinBox->setValue(value);
    }
    else if(comboBox != NULL)
    {
        QString value;
        value = index.model()->data(index, Qt::EditRole).toString();
        comboBox->setCurrentText(value);
    }
    else if(lineEdit != NULL)
    {
        QString value;
        value = index.model()->data(index, Qt::UserRole).toString();
        value = value.mid(2);
        lineEdit->setText(value);
    }
//    else if(dateEdit != NULL)
//    {
//        QDate date;
//        date.fromString(index.model()->data(index, Qt::EditRole).toString());
//        dateEdit->setDate(date);
//    }
    else QStyledItemDelegate::setEditorData(editor, index);
}

void EditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *lineEdit = qobject_cast <QLineEdit *>(editor);
    if(lineEdit != NULL)
        if(lineEdit->echoMode() != passEchoMode)
            lineEdit = NULL;

    if(lineEdit != NULL)
    {
        QMap<int, QVariant> roles;
        roles.insert(Qt::DisplayRole, QString(lineEdit->text().length(), QChar(0x25CF)));
        roles.insert(Qt::UserRole, QString("p;%1").arg(lineEdit->text()));
        model->setItemData(index, roles);
    }
    else
        QStyledItemDelegate::setModelData(editor, model, index);
}


/**
 * @brief EditDelegate::updateEditorGeometry updates editor's size when required
 * @param editor
 * @param option
 */
void EditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

