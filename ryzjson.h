/*
 * ryz.h - program do analizy ziaren ryżu
 *
 * Autor:  (2019) Piotr M. Szczypiński, Politechnika Łódzka
 */

#ifndef RYZjson_H
#define RYZjson_H

#include <QJsonObject>
void ryzJsonWrite(QJsonObject &json);
void ryzJsonRead(const QJsonObject &json);
int ryzLoadParameters(std::istream* file);
int ryzLoadParameters(const char* fileName);
#endif // RYZjson_H
